/**
 * @file KeyTest.cpp
 * @author MohammedAlanizy
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Item.h>
#include <Key.h>
#include <Game.h>

/** Mock class for testing the music Item */
class ItemMock : public Item
{
public:
    /**  Constructor
     * \param gamge game this tile is a member of */
    ItemMock(Game *game) : Item(game)
    {
    }
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     * */
    virtual void Accept(ItemVisitor* visitor) { }
    ///  Default constructor (disabled)
    ItemMock() = delete;

    ///  Copy constructor (disabled)
    ItemMock(const ItemMock &) = delete;

    ~ItemMock() {}
};

TEST(KeyTest, Construct) {
    Game game(nullptr);
    ItemMock item(&game);
}

TEST(KeyTest, GetKeyCode)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some keys
    auto key1 = std::make_shared<Key>(&game,65);
    auto key2 = std::make_shared<Key>(&game,78);
    auto key3 = std::make_shared<Key>(&game,118);
    auto key4 = std::make_shared<Key>(&game,64);



    game.Add(key1);
    game.Add(key2);
    game.Add(key3);
    game.Add(key4);


    // check key1
    ASSERT_EQ(key1->GetKeyCode(), 65);
    // check key2
    ASSERT_EQ(key2->GetKeyCode(), 78);
    // check key3
    ASSERT_EQ(key3->GetKeyCode(), 118);
    // check key4
    ASSERT_EQ(key4->GetKeyCode(), 64);
}