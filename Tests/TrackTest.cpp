/**
 * @file SoundBoardTest.cpp
 * @author MohammedAlanizy
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Item.h>
#include <Track.h>
#include <Game.h>
#include <Key.h>

/** Mock class for testing the music Item */
class ItemMock : public Item
{
public:
    /**  Constructor
     * \param gamge game this tile is a member of */
    ItemMock(Game *game) : Item(game)
    {
    }
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     * */
    virtual void Accept(ItemVisitor* visitor) { }
    ///  Default constructor (disabled)
    ItemMock() = delete;

    ///  Copy constructor (disabled)
    ItemMock(const ItemMock &) = delete;

    ~ItemMock() {}
};

TEST(TrackTest, Construct) {
    Game game(nullptr);
    ItemMock item(&game);
}

TEST(TrackTest, SetLine)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some key and track
    auto key = std::make_shared<Key>(&game,65);
    auto track = std::make_shared<Track>(&game,"i600",key);
    track->SetLine(10,20,30,40);

    game.Add(key);
    game.Add(track);


    // check x1
    ASSERT_EQ(track->GetX1(), 10);
    // check x2
    ASSERT_EQ(track->GetX2(), 20);
    // check y1
    ASSERT_EQ(track->GetY1(), 30);
    // check y2
    ASSERT_EQ(track->GetY2(), 40);
}