/**
 * @file GameTest.cpp
 * @author Abdulrahman
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Game.h>
#include <Image.h>
#include <SoundBoard.h>

using namespace std;


TEST(GameTest, Construct){
    Game game(nullptr);
}

TEST(GameTest, Iterator)
{
    // Construct a game object
    Game game(nullptr);

    // Add some images
    auto image1 = std::make_shared<Image>(&game);
    auto image2 = std::make_shared<Image>(&game);
    auto image3 = std::make_shared<Image>(&game);
    auto soundBoard = std::make_shared<SoundBoard>(&game);
    game.Add(image1);
    game.Add(image2);
    game.Add(image3);
    game.Add(soundBoard);

    // Begin points to the first item
    auto iter1 = game.begin();

    // End points after the last item
    auto iter2 = game.end();

    ASSERT_EQ(image1, *iter1) << L"First item correct";
    ++iter1;
    ASSERT_EQ(image2, *iter1) << L"Second item correct";
    ++iter1;
    ASSERT_EQ(image3, *iter1) << L"Third item correct";
    ++iter1;
    ASSERT_EQ(soundBoard, *iter1) << L"Fourth item correct";
    ++iter1;
    ASSERT_FALSE(iter1 != iter2);
}






