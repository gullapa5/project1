/**
 * @file SoundBoardTest.cpp
 * @author MohammedAlanizy
 */
#include <pch.h>
#include "gtest/gtest.h"
#include <Item.h>
#include <Game.h>
#include <Image.h>
#include <Track.h>
#include <Key.h>
#include <SoundBoard.h>
#include <SoundBoardFinderVisitor.h>

/** Mock class for testing the music Item */
class ItemMock : public Item
{
public:
    /**  Constructor
     * \param gamge game this tile is a member of */
    ItemMock(Game *game) : Item(game)
    {
    }
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     * */
    virtual void Accept(ItemVisitor* visitor) { }
    ///  Default constructor (disabled)
    ItemMock() = delete;

    ///  Copy constructor (disabled)
    ItemMock(const ItemMock &) = delete;

    ~ItemMock() {}
};


TEST(SoundBoardTest, Construct) {
    Game game(nullptr);
    ItemMock item(&game);
}

TEST(SoundBoardTest, FindSoundBoard)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some images
    auto image1 = std::make_shared<Image>(&game);
    auto image2 = std::make_shared<Image>(&game);
    auto image3 = std::make_shared<Image>(&game);
    auto soundBoard = std::make_shared<SoundBoard>(&game);
    soundBoard->SetId("i600");

    game.Add(image1);
    game.Add(image2);
    game.Add(image3);
    game.Add(soundBoard);

    SoundBoardFinderVisitor visitor;

    game.Accept(&visitor);


    ASSERT_EQ(visitor.GetSoundBoard()->GetId(), soundBoard->GetId());
}
TEST(SoundBoardTest, GetTopWidth)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some images
    auto image1 = std::make_shared<Image>(&game);
    auto image2 = std::make_shared<Image>(&game);
    auto image3 = std::make_shared<Image>(&game);
    auto soundBoard = std::make_shared<SoundBoard>(&game);
    soundBoard->SetTopWidth(10);

    game.Add(image1);
    game.Add(image2);
    game.Add(image3);
    game.Add(soundBoard);



    ASSERT_EQ(soundBoard->GetTopWidth(), 10);
}

TEST(SoundBoardTest, GetSizeBeats)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some images
    auto image1 = std::make_shared<Image>(&game);
    auto image2 = std::make_shared<Image>(&game);
    auto image3 = std::make_shared<Image>(&game);
    auto soundBoard = std::make_shared<SoundBoard>(&game);
    soundBoard->SetSizeBeats(23);

    game.Add(image1);
    game.Add(image2);
    game.Add(image3);
    game.Add(soundBoard);



    ASSERT_EQ(soundBoard->GetSizeBeats(), 23);
}

TEST(SoundBoardTest, AddTrack5)
{
    Game game(nullptr);
    ItemMock item(&game);
    // Add some images
    auto key = std::make_shared<Key>(&game,65);
    auto track1 = std::make_shared<Track>(&game,"i600",key);
    auto track2 = std::make_shared<Track>(&game,"i601",key);

    auto track3 = std::make_shared<Track>(&game,"i603",key);
    auto track4 = std::make_shared<Track>(&game,"i604",key);
    auto track5 = std::make_shared<Track>(&game,"i604",key);
    auto soundBoard = std::make_shared<SoundBoard>(&game);

    soundBoard->AddTrack(track1);
    soundBoard->AddTrack(track2);
    soundBoard->AddTrack(track3);
    soundBoard->AddTrack(track4);
    soundBoard->AddTrack(track5);

    game.Add(track1);
    game.Add(track2);
    game.Add(track3);
    game.Add(track4);
    game.Add(track5);
    game.Add(soundBoard);


    // Test up to five items ...
    ASSERT_EQ(soundBoard->GetTracks()[0], track1);
    ASSERT_EQ(soundBoard->GetTracks()[1], track2);
    ASSERT_EQ(soundBoard->GetTracks()[2], track3);
    ASSERT_EQ(soundBoard->GetTracks()[3], track4);
    ASSERT_EQ(soundBoard->GetTracks()[4], track5);
}