/**
 * @file LevelTest.cpp
 * @author Abdulrahman Alanazi
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Game.h>
#include <regex>
#include <string>
#include <fstream>
#include <streambuf>
#include <wx/filename.h>
using namespace std;

//class LevelTest : public ::testing::Test {
//protected:
//    Game game;
//    wxString xmlFilePath = L"levels/level0.xml";
//    bool AreGamesEqual(const Game& game1, const Game& game2) {
//        // Example comparison logic (you'll need to replace this with actual comparisons)
//        // For instance, if Game has a method to get the count of elements or a specific state you can compare
//        return game1.GetElementCount() == game2.GetElementCount();
//        // You should extend this function to compare other relevant properties or states
//    }
//
//};
//TEST_F(LevelTest, SuccessfulFileLoad) {
//
//    EXPECT_NO_THROW(level.LevelLoader(xmlFilePath, game));
//}
//TEST_F(LevelTest, NonExistentFile) {
//    EXPECT_THROW(level.LevelLoader("nonexistent_file_path", game), runtime_error);
//}
//
//TEST_F(LevelTest, InvalidFile) {
//    EXPECT_THROW(level.LevelLoader("images/background1.png", game), runtime_error);
//}
//TEST_F(LevelTest, TraversingFile){
//    Game game2;
//    ASSERT_EQ(game.GetElementCount(), game2.GetElementCount());
//    level.LevelLoader(xmlFilePath, game);
//    ASSERT_NE((game.GetElementCount()),(game2.GetElementCount()));
//    wxString xmlFilePath1 = L"levels/level1.xml";
//    wxString xmlFilePath2 = L"levels/level2.xml";
//    Game game3;
//    Game game4;
//    level.LevelLoader(xmlFilePath1,game3);
//    ASSERT_NE((game.GetElementCount()),(game3.GetElementCount()));
//    ASSERT_NE((game2.GetElementCount()),(game3.GetElementCount()));
//    ASSERT_EQ((game3.GetElementCount()),(game3.GetElementCount()));
//    level.LevelLoader(xmlFilePath2,game4);
//    ASSERT_NE((game.GetElementCount()),(game4.GetElementCount()));
//    ASSERT_NE((game2.GetElementCount()),(game4.GetElementCount()));
//    ASSERT_NE((game3.GetElementCount()),(game4.GetElementCount()));
//    ASSERT_EQ((game4.GetElementCount()),(game4.GetElementCount()));
//}




