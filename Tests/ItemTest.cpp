/**
 * @file ItemTest.cpp
 * @author Abdulrahman
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Item.h>
#include <Game.h>

/** Mock class for testing the class Item */
class ItemMock : public Item
{
public:
    /**  Constructor
     * \param gamge game this tile is a member of */
    ItemMock(Game *game) : Item(game)
    {
    }
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     * */
    virtual void Accept(ItemVisitor* visitor) override { }
    ///  Default constructor (disabled)
    ItemMock() = delete;

    ///  Copy constructor (disabled)
    ItemMock(const ItemMock &) = delete;

    ~ItemMock() {}
};

TEST(ItemTest, Construct) {
    Game game(nullptr);
    ItemMock item(&game);
}

TEST(ItemTest, SetLocation)
{
    Game game(nullptr);
    ItemMock item(&game);

    ASSERT_EQ(0, item.GetX());
    ASSERT_EQ(0, item.GetY());

    item.SetLocation(17, 23);
    ASSERT_EQ(17, item.GetX());
    ASSERT_EQ(23, item.GetY());
}

TEST(ItemTest, ResizeImage)
{
    Game game(nullptr);
    ItemMock item(&game);
    ASSERT_EQ(0, item.GetWidth());
    ASSERT_EQ(0, item.GetHeight());
    item.ResizeImage(17, 23);
    ASSERT_EQ(17, item.GetWidth());
    ASSERT_EQ(23, item.GetHeight());
}

TEST(ItemTest, SetId)
{
    Game game(nullptr);
    ItemMock item(&game);
    ASSERT_EQ("", item.GetId());
    item.SetId("i600");
    ASSERT_EQ("i600", item.GetId());
}

