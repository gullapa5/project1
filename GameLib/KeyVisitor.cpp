/**
 * @file KeyVisitor.cpp
 * @author MohammedAlanizy
 */

#include "pch.h"
#include "KeyVisitor.h"

/**
 * Assign the member variable to the key object
 * @param key Key object to assign it.
*/
void KeyVisitor::VisitKey(Key* key) {
    if (key != nullptr)
    {
        mKey = key;
    }
}
