/**
 * @file Declarations.cpp
 * @author MohammedAlanizy
 */



#include "pch.h"
#include <wx/tokenzr.h>
#include "Declarations.h"


/**
 * Declarations Constructor
 * @param node a pointer to the XmlNode
 * @param game a pointer to the game
 */

Declarations::Declarations(wxXmlNode *node, Game *game) : mDeclaration(node), mGame(game) {

}


/**
 * Get two numbers between comma this is usually used for size, position.
 * @param targetString the string that we want to get the two numbers from.
 * @return a wxSize that has x,y.
*/
wxSize Declarations::GetNumberBetweenComma(wxString targetString) {
    wxStringTokenizer tokenizer(targetString, ",");
    // Variables to store the numbers
    wxString token;
    int x = 0, y = 0;
    // Tokenize the string and extract each number
    if (tokenizer.HasMoreTokens()) {
        token = tokenizer.GetNextToken();
        token.ToInt(&x);
    }
    if (tokenizer.HasMoreTokens()) {
        token = tokenizer.GetNextToken();
        token.ToInt(&y);
    }
    return wxSize(x,y);
}

