/**
 * @file DeclarationMusic.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONMUSIC_H
#define PROJECT1_GAMELIB_DECLARATIONMUSIC_H
#include "Declarations.h"
#include "PlayingArea.h"
/**
 * handling the music declaration tag in the xml file
 */
class DeclarationMusic : public Declarations
{
private:

public:
    DeclarationMusic(wxXmlNode *node,Game* game);

    void CreateNote( wxXmlNode *node, SoundBoard* soundBoard);

    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override {};
};

#endif //PROJECT1_GAMELIB_DECLARATIONMUSIC_H
