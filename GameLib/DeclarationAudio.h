/**
 * @file DeclarationAudio.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_DECLARATIONAUDIO_H
#define PROJECT1_DECLARATIONAUDIO_H
#include "Declarations.h"
/**
 * handling the audio declaration tag in the xml file
 */
class DeclarationAudio : public Declarations
{
private:

public:
    /**
     * Constructor
     * @param node The node to the audio xml
     * @param game The pointer to the current game
     */
    DeclarationAudio(wxXmlNode *node,Game* game);

    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override {};
    /**
     * Creates the sound
     * @param node The node to the sound xml
     */
    void CreateSound(wxXmlNode *node);
};


#endif //PROJECT1_DECLARATIONAUDIO_H
