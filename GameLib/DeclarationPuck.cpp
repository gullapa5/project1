/**
 * @file DeclarationPuck.cpp
 * @author MohammedAlanizy
 */


#include "pch.h"
#include "DeclarationPuck.h"
#include "Image.h"
#include "SoundBoard.h"
#include "Game.h"
#include "SoundBoardFinderVisitor.h"


/**
 * DeclarationPuck Constructor
 */
DeclarationPuck::DeclarationPuck(wxXmlNode *node, Game* game) : Declarations(node, game){

}


/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return a puck that has been created.
*/
std::shared_ptr<Puck> DeclarationPuck::CreatePuck(wxXmlNode *node){
    // ** Declaration ** //
    std::wstring id =  GetDeclaration()->GetAttribute(L"id", L"i000").ToStdWstring();
    std::wstring imageName =  GetDeclaration()->GetAttribute(L"image").ToStdWstring();
    int trackNum;
    double tolerance;
    GetDeclaration()->GetAttribute(L"track", L"1").ToInt(&trackNum);
    GetDeclaration()->GetAttribute(L"tolerance", L"0.5").ToDouble(&tolerance);
    std::wstring imageSizeWithComma =  GetDeclaration()->GetAttribute(L"size", L"0,0").ToStdWstring();
    wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
    // ** Item ** //
    //  << - There is no item for Note ! - >> //
    // ** Initialization ** //
    auto newNote = std::make_shared<Puck>(GetGame(),imageSize.GetWidth(),imageSize.GetHeight(),trackNum ,tolerance);
    newNote->SetImage(imageName);
    newNote->SetId(id);
    newNote->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    return newNote;
}