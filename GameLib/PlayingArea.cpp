/**
 * @file PlayingArea.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "PlayingArea.h"
/// SecondsPerMinute for each beat
const int SecondsPerMinute = 60;

/// Time after the level done
const int TimeBeatAfterLevel = 2.0;

/// Score we get when we successfully hit a note
const int GoodSoundScore = 10;

/// The maximum bonus we can get for
/// holding for the duration for a long sound
const int MaxDurationBonus = 10;

/**
 * Constructor
 */

PlayingArea::PlayingArea()
{

}

/**
* Update every 30 frame
* @param elapsed elapsed after updated
*/
void PlayingArea::Update(double elapsed) {
    mBeat += elapsed * mBeatsPerMinute / SecondsPerMinute;
    mGame->UpdateTracks(elapsed);
    // to move to the next level
    double measure = mBeat / mGame->GetPlayingArea()->GetBeatsPerMeasure() + 1;
    if (measure > mMeasure + TimeBeatAfterLevel ){
        mGame->ResetSeconds();
        mGame->SetGameState(Game::GameStates::LevelComplete);
    }
    // play background music
    if (mBeat > 0){
        if (!(mGame->GetAudioManager()->GetAudio(GetBackgroundMusic())->IsPlaying())){
            mGame->GetAudioManager()->GetAudio(GetBackgroundMusic())->PlaySound();
        }
    }
}

/**
* Increment the score by GoodSoundScore
*/
void PlayingArea::IncrementScore() {
    mCorrectNotes ++;
    mScore += GoodSoundScore;
}

/**
* Bonus the score by GoodSoundScore
 * @param value the value the player got
 * @param duration the full duration
*/
void PlayingArea::BonusScore(double value, double duration) {
    mScore += value/duration*MaxDurationBonus;
}

/**
* Increment the note by adding to a vector
* @param note the notes
*/
void PlayingArea::IncreaseNoteCounts(std::shared_ptr<Note> note){
    if(std::find(mNotesTrash.begin(), mNotesTrash.end(), note) != mNotesTrash.end()) {
        // if the note has added before we will ignore the counter
    } else {
        // This list can be improved by delete all the item with update function, but we don't have time for performance improvement
        mNotesTrash.push_back(note);
        mAllNotesCount++;
    }
}