/**
 * @file Key.h
 * @author kraze
 * @author bdeaner
 *
 */

#ifndef PROJECT1_GAMELIB_KEY_H
#define PROJECT1_GAMELIB_KEY_H

#include "GameView.h"
#include "Item.h"
#include <set>

/**
 * handling the key items
 */
class Key : public Item {

private:
    /// number of keys
    int mKey;
    /// Time for the user holding the key
    double mHoldingTime;
    /// shared ptr of the currently playing keys
    std::shared_ptr<Note>  mCurrentPlaying;
    /// All of the keys that have been pressed
    std::set<int> mKeys;
    /// whether the key is highlights or not
    bool mHighlights = false;
    /// whether the key is in animation or not
    bool mAnimation = false;
public:
    /// A function for each key that has been pressed
     void KeyDown();
    /// A function for each key that has been holding
     void KeyHolding();
    /// A function for each key that has been released
     void KeyUp();
    /// A function for each key that has been released (after it was holding)
     void KeyUpHolding();
    /**
     * A function that add a key code to the set so we can
     * avoid repeating keys
     * @param newKeyCode The new key code that has been pressed
     */
    void NewKeyPressed(int newKeyCode){mKeys.insert(newKeyCode);};
    /**
     * A function that remove a keyCode from a set
     * @param oldKeyCode The old key code that has been released
     */
    void RemoveKeyPressed(int oldKeyCode){mKeys.erase(oldKeyCode);};
    bool HasKeyPressedBefore(int keyCode);
    /**
     * A function that return the current key.
     * @return int mKey
     */
    int GetKeyCode() {return mKey;};
    /**
     * Set whether has the features highlights or not.
     * @param enabled whether is enabled or not
     */
    void SetHighlights(bool enabled) {mHighlights = enabled;};
    /**
     * Set whether has the key has animation or not.
     * @param enabled whether is enabled or not
     */
    void SetAnimation(bool enabled) {mAnimation = enabled;};
    /**
     * Accept a visitor for a key
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) override {visitor->VisitKey(this);}
    /**
     *  Constructor
     *  @param game the current game
     *  @param key the int key
    */
    Key(Game* game,int key);
};



#endif //PROJECT1_GAMELIB_KEY_H
