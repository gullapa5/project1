/**
 * @file Game.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__GAME_H
#define PROJECT1__GAME_H

#include "Item.h"
#include <map>
#include "Puck.h"
#include <wx/wx.h>
#include <wx/xml/xml.h>

#include "Sound.h"
#include "Declarations.h"
#include "AudioManager.h"
/**
 * handling SpartanHero-related functionalities, including the management
 * of the background image through a unique pointer to a wxBitmap,
 * rendering the SpartanHero through its OnDraw method.
 */


class Game {
public:
    ///All possible game states
    enum class GameStates {LevelStart,Playing,LevelFailed, LevelComplete};
    ///Handle updates for animation
    void Update(double elapsed);
    ///Draw the application (backgorund).
    void OnDraw(std::shared_ptr<wxGraphicsContext> graphics,int width, int height);
    ///Game Construtor
    Game(ma_engine * audioEngine);
    ///Getter for audio engine
    ///@return Audio Engine
    ma_engine* GetAudioEngine(){return mAudioEngine;};
    ///Clear all the items in the game
    void NewGame();
    ///Setter for current lever
    ///@param levelNumber
    void SetCurrentLevel(int levelNumber){mCurrentLevel = levelNumber;};
    ///Returns playing area
    ///@param playingArea
    void SetPlayingArea(PlayingArea * playingArea){mPlayingArea =playingArea; };
    ///Setter for AudioManager
    ///@param audioManager
    void SetAudioManager(AudioManager * audioManager){mAudioManager =audioManager; };
    ///Getter for GameState
    ///@return mState
    GameStates GetGameState(){return mState;};
    ///Setter for GameState
    ///@param state
    void SetGameState(GameStates state ){mState = state;};

    ~Game();
    ///Loads sounds into our levels
    ///@param audioNode
    void LoadLevelSounds(wxXmlNode* audioNode);
    ///Loads level
    void LoadLevel(const wxString &filename);
    ///Adds an item to mItems
    void Add(std::shared_ptr<Item> item);
    ///Add function for items that take a
    ///@param item
    ///@param x
    ///@param y
    void AddLocation(std::shared_ptr<Item> item, int x, int y);
    ///Key press detector
    void KeyPressed(int keyCode);
    ///Key released detector
    void KeyUp(int keyCode);
    ///Getter for playing area
    ///@return mPlayingArea
    PlayingArea* GetPlayingArea(){return mPlayingArea;};
    ///Getter for audio manager
    ///@return mAudioManager
    AudioManager* GetAudioManager(){return mAudioManager;};
    /**
     * Get the directory the images are stored in
     * @return Images directory path
     */
    const std::wstring &GetImagesDirectory() const { return mImagesDirectory; }

    void SetImagesDirectory(const std::wstring &dir);

    void SetLevelsDirectory(const std::wstring &dir);
    ///Updates for animating tracks
    ///@param elapsed
    void UpdateTracks(double elapsed);
    ///Declares an xml file
    void XmlDeclaration(wxXmlNode* declerationNode,std::map<wxString, std::shared_ptr<Declarations>> &declerationMap);
    ///Creates an xml Item
    void XmlItem(wxXmlNode* itemsNode, std::map<wxString, std::shared_ptr<Declarations>> &declerationMap);
    ///Makes an xml audio node
    void XmlAudio(wxXmlNode* audioNode);
    ///Makes an xml music node
    void XmlMusic(wxXmlNode* musicNode);
    ///Inital hit test
    ///@param x
    ///@param y
    ///@return item
    std::shared_ptr<Item> HitTest(int x, int y);
    ///Getter for element count
    ///@return size_t
    size_t GetElementCount() const;
    ///Loader for sounds
    void LoadSounds();
    /** Iterator that iterates over the game */
    class Iter
    {
    public:
        /** Constructor
         * @param game The game we are iterating over
         * @param pos Position in the collection
         */
        Iter(Game* game, int pos) : mGame(game), mPos(pos) {}

        /**
         * Compare two iterators
         * @param other The other iterator we are comparing to
         * @return  true if this position is not equal to the other position
        */
        bool operator!=(const Iter& other) const
        {
            return mPos != other.mPos;
        }

        /**
         * Get value at current position
         * @return Value at mPos in the collection
         */
        std::shared_ptr<Item> operator *() const { return mGame->mItems[mPos]; }

        /**
         * Increment the iterator
         * @return Reference to this iterator */
        const Iter& operator++()
        {
            mPos++;
            return *this;
        }

    private:
        Game* mGame;   ///< Game we are iterating over
        int mPos;       ///< Position in the collection
    };
    ///Get an iterator for the beginning of the collection
    /// @return iter
    Iter begin() { return Iter(this, 0); }
    ///Sets the seconds variable back to 0
    void ResetSeconds(){mSeconds = 0;};
    ///Getter for current level
    /// @return int
    int GetCurrentLevel(){ return mCurrentLevel;}
    ///Getter for width
    /// @return width
    int GetWidth(){ return mWidth;}
    ///Getter for height
    /// @return heihgt
    int GetHeight(){ return mHeight;}
    ///Get an iterator for the end of the collection
    /// @return iter
    Iter end() { return Iter(this, mItems.size()); }
    ///Accept a visitor for the collection
    void Accept(ItemVisitor *visitor);
    ///Moves items to front of mItems
    ///@param item
    void MoveToFront(std::shared_ptr<Item> item);
    void DeleteItem(std::shared_ptr<Item> item);

private:
    /// All of the items to populate our Game
    std::vector<std::shared_ptr<Item>> mItems;
    int mWidth = 1304; ///< the default width is 1304
    int mHeight = 900; ///< the default height is 900
    double mScale = 0.0; ///< scale background image
    double mXOffset = 0.0; ///< offset for x background image
    double mYOffset = 0.0; ///< offset for y background image
    double mSeconds = 0.0; ///< the seconds of the current level


    PlayingArea* mPlayingArea; ///< Playing area

    bool mShowNoticeText = true; ///< showing the notice text when the game initialized
    /// Directory containing the system images
    std::wstring mImagesDirectory;
    /// Directory containing the system levels
    std::wstring mLevelsDirectory;
    /// The audio engine for miniaudio
    ma_engine *mAudioEngine; ///< audio engine

    AudioManager* mAudioManager; ///< audio manager
    int mCurrentLevel = 0; ///< cuttrent level
    ///Inital game state
    GameStates mState = GameStates::LevelStart;

};

#endif //PROJECT1__GAME_H