/**
 * @file ScoreBoardFinderVisitior.cpp
 * @author MohammedAlanizy
 */
#include "pch.h"
#include "ScoreBoardFinderVisitior.h"
/**
 * Assign the member variable to the scoreBaord object
 * @param scoreBaord scoreBaord object to assign it.
*/
void ScoreBoardFinderVisitior::VisitScoreBoard(ScoreBoard *scoreBaord)
{
    if(scoreBaord != nullptr)
    {
        mScoreBoard = scoreBaord;
    }
}