/**
 * @file DeclarationMusic.cpp
 * @author MohammedAlanizy
 */


#include "pch.h"
#include "DeclarationMusic.h"

#include "PlayingArea.h"
#include "Game.h"
#include "SoundBoard.h"
#include "Track.h"
#include "SoundBoardFinderVisitor.h"


/** Top clearance so we don't draw past the cover
 * as a fraction of the soundboard height. This
 * is where the pucks come in
*/
const double TopClearance = 0.1;

/**
 * DeclarationMusic Constructor
  * @param node a pointer to the XmlNode
 * @param game a pointer to game
 */
DeclarationMusic::DeclarationMusic(wxXmlNode *node,Game* game) : Declarations(node,game){

}

/**
 * Create note from the xml file.
 * @param node a pointer to the XmlNode.
 * @param soundBoard a pointer to the current Soundboard that we are working on it.
*/
void DeclarationMusic::CreateNote( wxXmlNode *node, SoundBoard* soundBoard){
    // ** Declaration ** //
    wxString id = node->GetAttribute(L"id", L"i000");
    auto currentPuck = soundBoard->GetPuck(id);
    wxString sound = node->GetAttribute(L"sound", L"A0");
    double beat;
    double duration;
    int measure;
    node->GetAttribute(L"beat", L"4").ToDouble(&beat);
    node->GetAttribute(L"measure", L"0").ToInt(&measure);
    node->GetAttribute(L"duration", L"0").ToDouble(&duration);

    auto newNote = std::make_shared<Note>(GetGame(),currentPuck->GetWidth(),currentPuck->GetHeight(),sound,beat,duration,measure,currentPuck->GetTrackNumber(),currentPuck->GetTolerance());
    newNote->SetId(id);
    newNote->SetImage(currentPuck->GetImageName().ToStdWstring());
    newNote->ResizeImage(currentPuck->GetWidth(),currentPuck->GetHeight());
    wxString trackNum =  wxString::Format("%d", newNote->GetTrackNumber());
    auto track = soundBoard->GetTrack(trackNum);
    newNote->SetLocation(track->GetX1(),track->GetY1());
    GetGame()->Add(newNote);
    track->AddNote(newNote);
}


/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclarationMusic::CreateItem(wxXmlNode *node){
    // ** Declaration ** //
    int beatsPerMinute;
    int beatsPerMeasure;
    int measures;
    GetDeclaration()->GetAttribute(L"beats-per-minute", L"132").ToInt(&beatsPerMinute);
    GetDeclaration()->GetAttribute(L"beats-per-measure", L"4").ToInt(&beatsPerMeasure);
    GetDeclaration()->GetAttribute(L"measures", L"0").ToInt(&measures);
    auto backing = GetDeclaration()->GetAttribute(L"backing", L"BACK");
    // ** Initialization ** //
    GetGame()->GetPlayingArea()->SetBeatsPerMeasure(beatsPerMeasure);
    GetGame()->GetPlayingArea()->SetBeatsPerMinute(beatsPerMinute);
    GetGame()->GetPlayingArea()->SetMeasure(measures);
    GetGame()->GetPlayingArea()->SetBackgroundMusic(backing);
    GetGame()->GetPlayingArea()->SetGame(GetGame());
    // ** Items ** //
    SoundBoardFinderVisitor visitor;
    GetGame()->Accept(&visitor);
    auto soundBoard = visitor.GetSoundBoard();
    for( ; node; node=node->GetNext()) {
        CreateNote(node,soundBoard);
    }
    // These two line below are required so the keys and soundboard cover comes after the puck
    for (auto track : soundBoard->GetTracks()){
        GetGame()->Add(track->GetKey());
    }
    GetGame()->Add(soundBoard->GetCoverImage());
    return nullptr;
}