/**
 * @file Track.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__TRACK_H
#define PROJECT1__TRACK_H

#include "Note.h"
#include "SoundBoard.h"

/**
 * Track class that manage the rendering of the Tracks
 */
class Track : public Item{

private:
    double mX1;             ///< X location for the center of the item

    double mX2;             ///< X location for the center of the item

    double mY1;             ///< Y location for the center of the item

    double mY2;             ///< Y location for the center of the item

    wxString mId;           ///< Identifier for the item

    wxString mImageName;    ///< Name of the image associated with the item

    std::shared_ptr<Item> mKey;  ///< Pointer to the associated key item

    std::vector<std::shared_ptr<Note>> mNotes;      ///< vector of all the notes

    std::map<std::shared_ptr<Note>, std::array<double, 4>> mLines;      ///< Map of the lines on the track

public:
//    void SetKey(std::shared_ptr<Key> key){mKey = key;};

    /** Constructor
     * @param game The game we are iterating over
     * @param id id of the item
     * @param key The type of key
     */
    Track(Game *game, wxString id,std::shared_ptr<Key> key);


    /**
    * Updates the program
    * @param elapsed time since starting
    */
    void Update(double elapsed) override;

    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    void Accept(ItemVisitor* visitor) override {};

    /**
    * Set line position
    * @param x1 starting point for x
    * @param x2 ending point for x
    * @param y1 starting point for y
    * @param y2 ending point for y
    */
    void SetLine(double x1,double x2, double y1, double y2){mX1=x1;mX2=x2;mY1=y1;mY2=y2;};

    /**
    * Get the double of the location
    * @return the double of the location
    */
    double GetX1(){return mX1;};

    /**
    * Get the double of the location
    * @return the double of the location
    */
    double GetX2(){return mX2;};

    /**
    * Get the double of the location
    * @return the double of the location
    */
    double GetY1(){return mY1;};

    /**
    * Get the double of the location
    * @return the double of the location
    */
    double GetY2(){return mY2;};

    /**
    * Get the notes
    * @return the vector of shared pointer notes
    */
    std::vector<std::shared_ptr<Note>> GetNotes(){return mNotes;};

    /**
    * Get the double of the location
    * @return the double of the location
    */
    std::shared_ptr<Item> GetKey(){return mKey;};

    /**
    * Adds a note
    * @param note the specified note
    */
    void AddNote(std::shared_ptr<Note> note){mNotes.push_back(note);};

    /**
    * Draws the track
    * @param graphics shared pointer to the wxGraphicsContext
    */
    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;
};

#endif //PROJECT1__TRACK_H
