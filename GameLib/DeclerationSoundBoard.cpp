/**
 * @file DeclerationSoundBoard.cpp
 * @author MohammedAlanizy
 */
#include "pch.h"
#include "DeclerationSoundBoard.h"
#include "DeclarationPuck.h"
#include "SoundBoard.h"
#include "Key.h"
#include "Track.h"
#include "Game.h"
#include "FindNoteDeclarationVisitor.h"
#include "Image.h"

/// The maximum number of tracks
const int MaxTracks = 10;

/// What is the border of the left and right
/// of the tracks? This is the percentage of
/// the space to the left of the first track line
/// and to the right of the last track line
const double Border = 0.075;

/// Top clearance so we don't draw past the cover
/// as a fraction of the soundboard height. This
/// is where the pucks come in
const double TopClearance = 0.1;

/// Percentage down on the soundboard to the keys
/// This is where the pucks go under the key and
/// disappear.
const double KeyRow = 0.85;

/**
 * DeclerationSoundBoard Constructor
 * @param node a pointer to the XmlNode
 * @param game a pointer to the game
 */
DeclerationSoundBoard::DeclerationSoundBoard(wxXmlNode *node,Game* game) : Declarations(node,game){

}


/**
 * It add tracks into the soundBoard; we find the tracks using node pointer
 * @param soundBoard The current soundboard being worked on
 * @param node a pointer to the track tag.
*/
void DeclerationSoundBoard::CreateTracks(std::shared_ptr<SoundBoard> soundBoard, wxXmlNode *node){
    for( ; node; node=node->GetNext()) {
        int id;
        auto idName = node->GetAttribute(L"track", L"1");
        node->GetAttribute(L"track", L"1").ToInt(&id);
        std::wstring imageName =  node->GetAttribute(L"key-image").ToStdWstring();
        std::wstring imageSizeWithComma =  node->GetAttribute(L"key-size", L"0,0").ToStdWstring();
        std::wstring highlights =  node->GetAttribute(L"highlights", L"false").ToStdWstring();
        std::wstring animation =  node->GetAttribute(L"animation", L"false").ToStdWstring();
        wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
        std::wstring key =  node->GetAttribute(L"key", L"A").ToStdWstring();
        auto newKey = std::make_shared<Key>(GetGame(),key[0]);
        newKey->SetImage(imageName);
        newKey->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
        newKey->SetId(idName);
        double leftOfTrack = soundBoard->GetX()/2;
        double rightOfTrack = soundBoard->GetX() + leftOfTrack;
        double startEdge = leftOfTrack +(soundBoard->GetX()*Border);
        double endEdge =  rightOfTrack - (soundBoard->GetX()*Border);
        double availableWidth = endEdge - startEdge;
        double trackWidth = availableWidth / (MaxTracks - 1);
        double x = startEdge +  (id - 1) * trackWidth ;
        double y = soundBoard->GetX();
        newKey->SetLocation(x,y);
        newKey->SetHighlights(highlights == "true");
        newKey->SetAnimation(animation == "true");
        auto newTrack = std::make_shared<Track>(GetGame(),idName,newKey);
        /// offset of the top width...
        leftOfTrack = (soundBoard->GetX()+soundBoard->GetTopWidth())/2;
        startEdge = leftOfTrack +(soundBoard->GetX()*Border);
        endEdge =  (soundBoard->GetX()+(soundBoard->GetX()-soundBoard->GetTopWidth())/2) - (soundBoard->GetX()*Border);
        availableWidth = endEdge - startEdge;
        trackWidth = availableWidth / (MaxTracks - 1);
        double x1 =  startEdge +  (id- 1) * trackWidth ;
        double x2 = x;
        double y1 = soundBoard->GetHeight() - soundBoard->GetTopWidth();
        double y2 = y;
        newTrack->SetId(idName);
        newTrack->SetLine(x1,x2,y1,y2);
        /// Add it both in the game and in the soundBoard AS A (POINTER) not a new item.
        GetGame()->Add(newTrack);
//        GetGame()->Add(newKey);
        soundBoard->AddTrack(newTrack);
    }
}


/**
 * It add pucks into the soundBoard; we find the puck using a visitor class
 * @param soundBoard current soundBoard the we want add a pucks to it.
*/
void DeclerationSoundBoard::CreatePucks(std::shared_ptr<SoundBoard> soundBoard){
    // Create a vistor to find all pucks in the decleration vector
    FindNoteDeclarationVisitor visitor;
    for (auto decleration : GetDeclarationsItems()){
        decleration.second->Accept(&visitor);
    }
    // Now we will get all pucks and start to add them into the soundboard.
    auto pucks = visitor.GetAllPucks();
    for (auto puck: pucks){
        // We will create the item with nullptr as the note (puck) doesn't have any children properties
        // I used dynamic_pointer_cast to convert the Item To puck, so we can add it
        auto newPuck = puck->CreatePuck(nullptr);
        soundBoard->AddPuck(newPuck->GetId(),newPuck);
    }

}


/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclerationSoundBoard::CreateItem(wxXmlNode *node){
    /// ** Declaration ** //
    int topWidth;
    int sizeBeats;
    std::wstring imageName =  GetDeclaration()->GetAttribute(L"image").ToStdWstring();
    std::wstring coverName=  GetDeclaration()->GetAttribute(L"cover").ToStdWstring();
    std::wstring imageSizeWithComma =  GetDeclaration()->GetAttribute(L"size", L"0,0").ToStdWstring();
    GetDeclaration()->GetAttribute(L"top-width", L"0").ToInt(&topWidth);
    GetDeclaration()->GetAttribute(L"size-beats", L"0").ToInt(&sizeBeats);
    wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
    /// ** Item ** //
    std::wstring SoundBoardPositionWithComma =  node->GetAttribute(L"p", L"0,0").ToStdWstring();
    wxSize SoundBoardPosition = GetNumberBetweenComma(SoundBoardPositionWithComma);
    /// ** Initialization ** //
    auto newSoundBoard = std::make_shared<SoundBoard>(GetGame());
    newSoundBoard->SetImage(imageName);
    newSoundBoard->SetTopWidth(topWidth);
    newSoundBoard->SetSizeBeats(sizeBeats);
    newSoundBoard->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newSoundBoard->SetLocation(SoundBoardPosition.GetX(),SoundBoardPosition.GetY());

    /// We will override this as the soundBoard needs to be drawn first
    GetGame()->Add(newSoundBoard);

    /// ** item -> track ** //
    CreateTracks(newSoundBoard,node->GetChildren()); // Add tracks

    /// ** item -> pucks (note in xml file)  ** //
    CreatePucks(newSoundBoard);

    /// ** SoundBoard Cover ** //
    auto newSoundBoardCover = std::make_shared<Image>(GetGame());
    newSoundBoardCover->SetImage(  coverName);
    newSoundBoardCover->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newSoundBoardCover->SetLocation(SoundBoardPosition.GetX(),SoundBoardPosition.GetY());

    newSoundBoard->SetCoverImage(newSoundBoardCover);

    return nullptr;
}


