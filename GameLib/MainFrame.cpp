/**
 * @file MainFrame.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "ids.h"
#include "MainFrame.h"
#include "GameView.h"
/**
 * Initialize the MainFrame window.
 * @param audioEngine
 */
void MainFrame::Initialize(ma_engine *audioEngine)
{
    Create(nullptr, wxID_ANY, L"Game", wxDefaultPosition,  wxSize( 1225,900 ));

    // Create a sizer that will lay out child windows vertically
    // one above each other
    auto sizer = new wxBoxSizer( wxVERTICAL );

    // Create the view class object as a child of MainFrame
    mGameView = new GameView(audioEngine);
    mGameView->Initialize(this);
    // Add it to the sizer
    sizer->Add(mGameView,1, wxEXPAND | wxALL );

    // Set the sizer for this frame
    SetSizer(sizer);

    // Layout (place) the child windows.
    Layout();
    // Add a menu bar
    auto menuBar = new wxMenuBar();
    auto fileMenu = new wxMenu();
    auto levelMenu = new wxMenu();
    auto helpMenu = new wxMenu();
    fileMenu->Append(wxID_CLOSE, "E&xit\tAlt-X", "Quit this program");
    levelMenu->Append(IDM_Level0, L"&Level 0", L"Load Level 0");
    levelMenu->Append(IDM_Level1, L"&Level 1", L"Load Level 1");
    levelMenu->Append(IDM_Level2, L"&Level 2", L"Load Level 2");
    levelMenu->Append(IDM_Level3, L"&Level 3", L"Load Level 3");
    levelMenu->AppendSeparator();
    levelMenu->Append(IDM_AUTOPLAY, L"&Auto Play", L"Turn on/off Auto Play mode",wxITEM_CHECK);
    helpMenu->Append(wxID_INFO, "&About\tF1", "Show about dialog");
    menuBar->Append(fileMenu, L"&File" );
    menuBar->Append(levelMenu, L"&Level" );
    menuBar->Append(helpMenu, L"&Help");
    SetMenuBar( menuBar );
    // A status bar
    CreateStatusBar( );
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnExit, this, wxID_CLOSE);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnAbout, this, wxID_INFO);
    // Uncomment when implemented the load functions and AutoPlayMode ...
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::LoadLevelOption, this, IDM_Level0);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::LoadLevelOption, this, IDM_Level1);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::LoadLevelOption, this, IDM_Level2);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::LoadLevelOption, this, IDM_Level3);
 //   Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::AutoPlayMode, this, IDM_AUTOPLAY);
}

/**
 * Exit menu option handlers
 * @param event
 */
void MainFrame::OnExit(wxCommandEvent& event)
{
    Close(true);
}
/**
 * Load level option
 * @param event
 */
void MainFrame::LoadLevelOption(wxCommandEvent &event){
    mGameView->LoadGame( event.GetId() - wxID_HIGHEST);
}
/**
 * About menu option handlers
 * @param event
 */
void MainFrame::OnAbout(wxCommandEvent& event)
{
    wxMessageBox(L"Welcome to the Game!",
                 L"About Game",
                 wxOK,
                 this);
}