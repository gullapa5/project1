/**
 * @file DeclarationVisitor.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONVISITOR_H
#define PROJECT1_GAMELIB_DECLARATIONVISITOR_H

class DeclarationPuck;
class DeclarationAudio;
/**
 * Declaration visitor base class
 */
class DeclarationVisitor
{
protected:
    /**
     * Constructor
     * Ensures this is an abstract class
     */
    DeclarationVisitor() {}

public:
    /**
     * Deconstruct
     */
    virtual ~DeclarationVisitor() {}

    /**
     * Visit a note object
     * @param note the note we are visiting
     */
    virtual void VisitPuck(DeclarationPuck* note) {};
};

#endif //PROJECT1_GAMELIB_DECLARATIONVISITOR_H
