/**
 * @file Sound.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SOUND_H
#define PROJECT1_GAMELIB_SOUND_H
#include <string>
#include "Item.h"
#include "miniaudio.h" // Ensure this path is correct based on your project setup
/**
 * Sound class for items
 */
class Sound : public Item{
public:
    /**
     * Constructor that loads a sound file.
     *
     * @param game the game object
     * @param filename The path to the sound file.
     * @param audioEngine A pointer to the initialized MiniAudio engine.
     * @param isLong is the audio long or not
     * @param volume the volume of the audio
     */
    Sound(Game *game,std::wstring filename, ma_engine* audioEngine, bool isLong,double volume);
    /**
     * Destructor to clean up resources.
     */
    ~Sound();

    /**
     * Plays the loaded sound from the beginning.
     */
    void PlaySound();

    /**
     * Stops the currently playing sound.
     */
    void PlayEnd();
    /**
    * Get is long
    * @return whether is long or not
    */
    bool GetIsLong(){return mLong;};
    /**
    * is the sound playing or not
    * @return whether is playing or not
    */
    bool IsPlaying(){return ma_sound_is_playing(&mSound);};

    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(ItemVisitor* visitor) {};

private:
    bool mLong = false;  ///< is the audio long.
    float mVolume = 1.0f;  ///< The volume sound object.
    ma_sound mSound; ///< The MiniAudio sound object.
    bool mLoaded = false; ///< Flag indicating whether the sound has been successfully loaded.
};

#endif //PROJECT1_GAMELIB_SOUND_H