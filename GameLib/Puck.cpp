/**
 * @file Puck.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "Puck.h"



/**
 * Constructor
 * @param game game object of puck
 * @param width width  of puck
 * @param height height of puck
 * @param track track of puck
 * @param tolerance tolerance of puck
 */
Puck::Puck(Game *game,int width,int height,int track,double tolerance)
: Item(game),mWidth(width), mHeight(height), mTrack(track), mTolerance(tolerance)
{

}
/**
 * Draw the scoreboard
 * @param graphics the graphics object
 */
void Puck::Draw(std::shared_ptr<wxGraphicsContext> graphics) {
    if (mShow)
    {
        Item::Draw(graphics);
    }
}