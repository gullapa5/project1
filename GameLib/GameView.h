/**
 * @file GameView.h
 * @author kraze
 * @author bdeaner
 *
 */

#ifndef PROJECT1__GAMEVIEW_H
#define PROJECT1__GAMEVIEW_H


#include "ids.h"
#include "Game.h"

/**
 *  representation of the SpartanHero view that inherits from wxWindow,
 *  handling the graphical display and interactive functionalities
 */
class GameView : public wxWindow
{
private:
    void OnPaint(wxPaintEvent &event);
    void OnTimer(wxTimerEvent& event);
    ///The current game
    Game mGame;
    /// The timer that allows for animation and the movement of the items
    wxTimer mTimer;
    /// The last stopwatch time
    long mTime = 0;
    /// Stopwatch used to measure elapsed time
    wxStopWatch mStopWatch;
public:
    GameView(ma_engine *audioEngine);
    /**
 *  Initialize the view of the game
     *  @param parent the default frame
 */
    void Initialize(wxFrame *parent);
    void OnExit(wxCommandEvent &event);
    void OnKeyDown(wxKeyEvent& event);
    void OnKeyUp(wxKeyEvent& event);
    void AutoPlayMode(wxCommandEvent& event);
    /**
 *  Loads the levels of the game
     *  @param levelNumber the level number
 */
    void LoadGame(int levelNumber);
    void UpdateTime();
};

#endif //PROJECT1__GAMEVIEW_H
