/**
 * @file ItemVisitor.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_ITEMVISITOR_H
#define PROJECT1_GAMELIB_ITEMVISITOR_H

class Key;
class SoundBoard;
class Puck;
class PlayingArea;
class ScoreBoard;
/**
 * Item visitor base class
 */
class ItemVisitor
{
protected:
    /**
     * Constructor
     * Ensures this is an abstract class
     */
    ItemVisitor() {}

public:
    virtual ~ItemVisitor() {}

    /**
     * Visit a Key object
     * @param key the key we are visiting
     */
    virtual void VisitKey(Key* key) {};
    /**
     * Visit SoundBoard object
     * @param soundBoard the soundBoard we are visiting
     */
    virtual void VisitSoundBoard(SoundBoard* soundBoard) {};
    /**
     * Visit Puck object
     * @param puck the puck we are visiting
     */
    virtual void VisitPuck(Puck* puck) {};
    /**
     * Visit PlayingArea object
     * @param music the music we are visiting
     */
    virtual void VisitMusic(PlayingArea* music) {};
    /**
     * Visit ScoreBoard object
     * @param scoreBoard the scoreBaord we are visiting
     */
    virtual void VisitScoreBoard(ScoreBoard* scoreBoard) {};
};

#endif //PROJECT1_GAMELIB_ITEMVISITOR_H
