/**
 * @file Needle.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_NEEDLE_H
#define PROJECT1_GAMELIB_NEEDLE_H

#include "Meter.h"

/**
 * handling the needle
 */
class Needle : public Meter
{
private:
public:
    /// Default constructor (disabled)
    Needle() = delete;

    /// Copy constructor (disabled)
    Needle(const Needle &) = delete;

    /// Assignment operator
    void operator=(const Needle &) = delete;

    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;


    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override {};
    /// Default constructor
    Needle(Game *game);
};

#endif //PROJECT1_GAMELIB_NEEDLE_H
