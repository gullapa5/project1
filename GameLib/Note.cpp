/**
 * @file Note.cpp
 * @author MohammedAlanizy
 */

#include "pch.h"
#include "Note.h"


/**
 * Constructor
 * @param game game object
 * @param width width of the note
 * @param height height of the note
 * @param soundName sound name of the note
 * @param beat beat of the note
 * @param duration duration of the note
 * @param measure measure of the note
 * @param track track of the note
 * @param tolerance tolerance of the note
 */
Note::Note(Game *game, int width, int height,wxString soundName,double beat, double duration,int measure,int track, double tolerance)
    : Puck(game,width,height,track,tolerance), mMeasure(measure), mSoundName(soundName), mBeat(beat), mDuration(duration){

}