/**
 * @file AudioManager.h
 * @author MohammedAlanizy
 *
 *
 */
#ifndef PROJECT1_AUDIOMANAGER_H
#define PROJECT1_AUDIOMANAGER_H

#include "Sound.h"
/**
 * handling the Audio
 */
class AudioManager {
private:
///The map of all the audios
    std::map<wxString, std::shared_ptr<Sound>> mAudios;
public:
    /**
     * Add a new audio
     * @param name The name of the audio
     * @param sound the sound object
     */
    void AddAudio(wxString name, std::shared_ptr<Sound> sound){mAudios[name]=sound;};


    /**
     * Clear the audios
     */
    void Clear();

    /**
     * Get an audio by its name .
     * @param name The name of the audio.
     * @return sound object if found.
     */
    std::shared_ptr<Sound> GetAudio(wxString name){return mAudios[name];};
    /// Default constructor
    AudioManager();
};


#endif //PROJECT1_AUDIOMANAGER_H
