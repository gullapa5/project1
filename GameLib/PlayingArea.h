/**
 * @file PlayingArea.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__MUSIC_H
#define PROJECT1__MUSIC_H
#include "Game.h"
/**
 * PlayingArea class that manage the different and related functionality of game PlayingArea
 */

class PlayingArea{
private:
    bool mAutoPlay = false; ///< whether auto play is on / off
    Game * mGame; ///< Game reference
    int mBeatsPerMinute = 0; ///< beats per minute
    int mBeatsPerMeasure = 0;  ///< beats per measure
    int mMeasure = 0;  ///< measure
    double mBeat = 1;  ///< current beat
    double mAllNotesCount = 0;  ///< all notes counter
    double mCorrectNotes = 0;  ///< all correct notes
    wxString mBacking;  ///< background music
    std::vector<std::shared_ptr<Note>> mNotesTrash;  ///< This used to delete the notes that has played before

    int mScore;  ///< current score
public:

    /// Copy constructor (disabled)
    PlayingArea(const PlayingArea &) = delete;

    /// Assignment operator
    void operator=(const PlayingArea &) = delete;

    /**
     * Start a level
     */
    void StartLevel() {mBeat=-mBeatsPerMeasure;mScore = 0;mAllNotesCount=0;mCorrectNotes=0;};
    /**
     * To switch on/off auto play
     * @param autoPlay whether on or off
     */
    void ToggleAutoPlay(bool autoPlay) {mAutoPlay = autoPlay;};

    /**
     * Get beats per measure
     * @return beats per measure
     */
    int GetBeatsPerMeasure(){return mBeatsPerMeasure;};
    /**
     * Get Auto Play mode
     * @return whether the auto play mode is on or off
     */
    int GetAutoPlayMode(){return mAutoPlay;};
    /**
     * Get Current beat
     * @return beat
     */
    double GetCurrentBeat(){return mBeat;};
    /**
     * Get the size correct notes that has been hit
     * @return double
     */
    double GetCorrectNote(){return mCorrectNotes;};
    /**
     * Get the size of note trash which is the notes that has been displayed before
     * @return int
     */
    double GetAllNotesCount(){return mAllNotesCount;};

    void IncreaseNoteCounts(std::shared_ptr<Note> note);
    /**
     * Set beats per measure
     * @param beatsPerMeasure per measure
     */
    void SetBeatsPerMeasure(int beatsPerMeasure){mBeatsPerMeasure = beatsPerMeasure;};
    /**
     * Get beats per Minute
     * @return beats per Minute
     */
    int GetBeatsPerMinute(){return mBeatsPerMinute;};
    /**
     * Set beats per Minute
     * @param beatsPerMinute per Minute
     */
    void SetBeatsPerMinute(int beatsPerMinute){mBeatsPerMinute = beatsPerMinute;};
    /**
     * Get measure
     * @return measure
     */
    int GetMeasure(){return mMeasure;};
    /**
     * Set measures
     * @param measures
     */
    void SetMeasure(int measures){mMeasure = measures;};

    void IncrementScore();

    void BonusScore(double value, double duration);
    /**
     * Get score
     * @return score
     */
    int GetScore(){return mScore;};
    /**
     * Get background music name
     * @return name of music
     */
    wxString GetBackgroundMusic(){return mBacking;};
    void Update(double elapsed);

    /**
     * Set the game
     * @param game game object
     */
    void SetGame(Game* game) {mGame=game;};
    /**
     * Set background music name
     * @param name name of music
     */
    void SetBackgroundMusic(wxString name){mBacking = name;};

    PlayingArea();
};

#endif //PROJECT1__MUSIC_H
