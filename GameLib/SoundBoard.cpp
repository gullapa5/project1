/**
 * @file SoundBoard.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "SoundBoard.h"
#include "Track.h"
#include "Game.h"


using namespace std;




/**
 * Constructor
 * @param game Game this SoundBoard is a member of
 */
SoundBoard::SoundBoard(Game *game) : Item(game)
{

}

/**
 * Update the Soundboard from the last update
 * @param elapsed time since last update
 */
void SoundBoard::Update(double elapsed) {

}

/**
 * It gets a track using its index
 * @param id id of the track
 * @return track
 */
std::shared_ptr<Track> SoundBoard::GetTrack(wxString id){
    for (auto track : mTracks){
        if (track->GetId() == id)
            return track;
    }
    // Not found
    return nullptr;
}
