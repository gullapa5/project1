/**
 * @file DeclerationScoreBoard.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLERATIONSCOREBOARD_H
#define PROJECT1_GAMELIB_DECLERATIONSCOREBOARD_H
#include "Declarations.h"
/**
 * handling the scoreboard declaration tag in the xml file
 */
class DeclerationScoreBoard : public Declarations
{
private:

public:
    DeclerationScoreBoard(wxXmlNode *node,Game* game);

    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override {};
};
#endif //PROJECT1_GAMELIB_DECLERATIONSCOREBOARD_H
