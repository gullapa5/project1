/**
 * @file AudioManager.cpp
 * @author MohammedAlanizy
 *
 *
 */

#include "pch.h"
#include "AudioManager.h"

/**
 * Constructor
 */
AudioManager::AudioManager()
{

}

void AudioManager::Clear() {
    // loop for each sound and if  it was playing we will stop it
    for (auto sound : mAudios){
        if (sound.second->IsPlaying())
            sound.second->PlayEnd();
    }
    // clean list
    mAudios.clear();
}
