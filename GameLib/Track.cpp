/**
 * @file Track.cpp
 * @author Abdulrahman Alanazi
 */

#include "pch.h"
#include "Track.h"
#include "Key.h"
#include "Sound.h"
#include "SoundBoardFinderVisitor.h"
#include <wx/graphics.h>
#include <array>
#include "PlayingArea.h"

/// Width of the track line. The line is
/// drawn a wxBLACK.
const int TrackLineWidth = 5;

/// Width of the long duration lines. These
/// lines are drawn as wxRED
const int LongDurationLineWidth = 12;


/// How much the image will be shrink
const double ShrinkImage = 2.0;

Track::Track(Game *game, wxString id,std::shared_ptr<Key> key)
    : Item(game), mId(id), mKey(key)
{

}

void Track::Update(double elapsed) {
//    Item::Update(elapsed);

    SoundBoardFinderVisitor visitor;
    GetGame()->Accept(&visitor);
    auto soundBoard = visitor.GetSoundBoard();
    for (auto note:  mNotes){
        double absoluteBeat = (note->GetMeasure() - 1.0) * GetGame()->GetPlayingArea()->GetBeatsPerMeasure() + (note->GetBeat() - 1.0);
        double distance = (absoluteBeat - GetGame()->GetPlayingArea()->GetCurrentBeat());
        if (distance <= soundBoard->GetSizeBeats() && distance >= 0)
        {
            if (GetGame()->GetPlayingArea()->GetAutoPlayMode())  // check if autoplay mode is on; then it plays itself
            {
                if ((!note->DidScoreTaken()) &&  distance <= note->GetTolerance()) {
                    GetGame()->GetAudioManager()->GetAudio(note->GetSoundName())->PlaySound();
                    GetGame()->GetPlayingArea()->IncrementScore();
                    note->TakeScore();
                    break;
                }
            }
            double x = mX2 - distance / soundBoard->GetSizeBeats() * (mX2 - mX1); // mX2 = top of the track, mX1 = bottom of the track
            double y = mY2 - distance / soundBoard->GetSizeBeats() * (mY2 - mY1); // mY2 = top of the track, mY1 = bottom of the track
            int width = note->GetBaseWidth() - distance / soundBoard->GetSizeBeats() * (note->GetBaseWidth() - note->GetBaseWidth()/ShrinkImage);
            int height = note->GetBaseWidth() - distance / soundBoard->GetSizeBeats() * (note->GetBaseWidth() - note->GetBaseWidth()/ShrinkImage);
            note->ResizeImage(width, height);
            note->SetLocation(x, y);
            note->Show(true);
        } else if (distance<= 0)
        {
            // the note out of bound
            GetGame()->GetPlayingArea()->IncreaseNoteCounts(note);
            note->Show(false);
        }else{
            note->Show(false);
        }
        // ** calculate the duration ** //
        // check if it 's long
        bool isLong = false;
        if (GetGame()->GetAudioManager()->GetAudio(note->GetSoundName()) != nullptr){ // we check just in case ...
            isLong = GetGame()->GetAudioManager()->GetAudio(note->GetSoundName())->GetIsLong();
        }
        if (isLong)
        {
            distance = ((absoluteBeat + note->GetDuration()) - GetGame()->GetPlayingArea()->GetCurrentBeat());
            double x2 = note->GetX();
            double y2 = note->GetY();
            double x1;
            double y1;
            if(distance <= soundBoard->GetSizeBeats())
            {
                if(distance >= 0)
                {
                    x1 = mX2 - distance / soundBoard->GetSizeBeats() * (mX2 - mX1); // mX2 = top of the track, mX1 = bottom of the track
                    y1 = mY2 - distance / soundBoard->GetSizeBeats() * (mY2 - mY1); // mY2 = top of the track, mY1 = bottom of the track
                }
                else
                {
                    x1 = mX2;
                    y1 = mY2;
                }
            }
            else
            {
                x1 = mX1; // mX2 = top of the track, mX1 = bottom of the track
                y1 = mY1; // mY2 = top of the track, mY1 = bottom of the track
            }
            // x1 , y1 , x2 , y2
            std::array<double, 4> coordinates = {x1, y1, x2, y2};
            mLines[note] = coordinates;
        }
    }
}

void Track::Draw(std::shared_ptr<wxGraphicsContext> graphics){
    // Draw track lines
    graphics->SetPen(wxPen(wxColor(*wxBLACK), TrackLineWidth,wxPENSTYLE_SOLID));
    graphics->StrokeLine(mX1, mY1, mX2, mY2);
    for (auto coordinate : mLines){
        graphics->SetPen(wxPen(wxColor(*wxRED), LongDurationLineWidth,wxPENSTYLE_SOLID));
        graphics->StrokeLine(coordinate.second[0], coordinate.second[1], coordinate.second[2], coordinate.second[3]);
    }
}

