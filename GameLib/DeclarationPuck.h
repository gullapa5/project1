/**
 * @file DeclarationPuck.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONPUCK_H
#define PROJECT1_GAMELIB_DECLARATIONPUCK_H

#include "Declarations.h"
/**
 * handling the puck declaration tag in the xml file
 */
class DeclarationPuck : public Declarations
{
private:

public:
        /**
     * Constructor
     * @param node The node to the audio xml
     * @param game The pointer to the current game
     */
    DeclarationPuck(wxXmlNode *node, Game* game);
    /**
     * Creates the puck
     * @param node node to the xml file
     * @return the shared ptr to the new created item
     */
    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override { return nullptr;};
    std::shared_ptr<Puck> CreatePuck(wxXmlNode *node);
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override { visitor->VisitPuck(this);};
};

#endif //PROJECT1_GAMELIB_DECLARATIONPUCK_H
