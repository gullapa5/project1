/**
 * @file Meter.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "Meter.h"
#include "Game.h"
#include <string>

using namespace std;


/// Score filename
//const wstring MeterBackImageName = L"images/meter-back.png";

/**
 * Constructor
 * @param game Game this guitar guy is a member of
 */
Meter::Meter(Game *game) : Score(game)
{

}


/*Meter::Meter(double width, double height, double centerX, double centerY) : Score(game, MeterBackImageName){
    meterWidth = width;
    meterHeight = height;
    meterCenterX = centerX;
    meterCenterY = centerY;
}*/


/*
void Meter::Draw(double notesHit, double totalNotes, wxGraphicsContext* graphics) {

    double performanceRatio = notesHit / totalNotes;
    double needleRotation = performanceRatio * MaxNeedleRotation * 2 - MaxNeedleRotation;

    // Draw background
    graphics->DrawBitmap(meterBackgroundImage->AsBitmap(), meterCenterX - meterWidth / 2, meterCenterY - meterHeight / 2, meterWidth, meterHeight);

    // Draw needle
    int needlePivotY = (int)(meterHeight * NeedlePivotYOffset);
    graphics->PushState();
    graphics->Translate(meterCenterX, meterCenterY + needlePivotY - meterHeight / 2);
    graphics->Rotate(needleRotation);
    graphics->DrawBitmap(needleImage->AsBitmap(), -meterWidth / 2, -needlePivotY, meterWidth, meterHeight);
    graphics->PopState();

    // Draw cover
    graphics->DrawBitmap(meterCoverImage->AsBitmap(), meterCenterX - meterWidth / 2, meterCenterY - meterHeight / 2, meterWidth, meterHeight);
}*/
