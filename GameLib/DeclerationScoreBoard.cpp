/**
 * @file DeclerationScoreBoard.cpp
 * @author MohammedAlanizy
 */


#include "pch.h"
#include "DeclerationScoreBoard.h"
#include "ScoreBoard.h"
#include "Game.h"



/**
 * DeclerationScoreBoard Constructor
 * @param node a pointer to the XmlNode
 * @param game a pointer to the game
 */
DeclerationScoreBoard::DeclerationScoreBoard(wxXmlNode *node,Game* game) : Declarations(node,game){

}



/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclerationScoreBoard::CreateItem(wxXmlNode *node){
    /// ** Declaration ** //
    std::wstring imageName =  GetDeclaration()->GetAttribute(L"image").ToStdWstring();
    std::wstring imageSizeWithComma =  GetDeclaration()->GetAttribute(L"size", L"0,0").ToStdWstring();
    wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
    /// ** Item ** //
    std::wstring ScoreBoardPositionWithComma =  node->GetAttribute(L"p", L"0,0").ToStdWstring();
    wxSize ScoreBoardPosition = GetNumberBetweenComma(ScoreBoardPositionWithComma);
    /// ** Initialization ** //
    auto newScoreBoard = std::make_shared<ScoreBoard>(GetGame());
    newScoreBoard->SetImage(imageName);
    newScoreBoard->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newScoreBoard->SetLocation(ScoreBoardPosition.GetX(),ScoreBoardPosition.GetY());
    return newScoreBoard;
}