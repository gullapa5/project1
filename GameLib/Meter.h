/**
 * @file Meter.h
 * @author Abdulrahman Alanazi
 *
 *
 */

#ifndef PROJECT1_GAMELIB_METER_H
#define PROJECT1_GAMELIB_METER_H


#include "Item.h"
#include "Score.h"
/**
 * handling the meter
 */
class Meter : public Score{

private:

public:



    /// Default constructor (disabled)
    Meter() = delete;

    /// Copy constructor (disabled)
    Meter(const Meter &) = delete;

    /// Assignment operator
    void operator=(const Meter &) = delete;

    Meter(Game* game);
    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    void Accept(ItemVisitor* visitor) override {};

};
#endif //PROJECT1_GAMELIB_METER_H
