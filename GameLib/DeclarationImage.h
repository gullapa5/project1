/**
 * @file DeclarationImage.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONIMAGE_H
#define PROJECT1_GAMELIB_DECLARATIONIMAGE_H

#include "Declarations.h"
/**
 * handling the image declaration tag in the xml file
 */
class DeclarationImage : public Declarations
{
private:

public:

    DeclarationImage(wxXmlNode *node,Game* game);

    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override {};
};

#endif //PROJECT1_GAMELIB_DECLARATIONIMAGE_H
