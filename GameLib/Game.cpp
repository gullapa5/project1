/**
 * @file Game.cpp
 * @author Abdulrahman Alanazi
 * @author MohammedAlanizy
 */
#include "pch.h"
#include "Game.h"
#include "KeyVisitor.h"
#include <wx/graphics.h>
#include <wx/tokenzr.h>

#include "Sound.h"
#include "AudioManager.h"
#include "DeclarationImage.h"
#include "DeclerationSoundBoard.h"
#include "DeclerationScoreBoard.h"
#include "ScoreBoardFinderVisitior.h"
#include "ScoreBoard.h"
#include "DeclarationMeter.h"
#include "DeclarationAudio.h"
#include "DeclarationPuck.h"
#include "DeclarationMusic.h"
using namespace std;

/// Directory containing the project images
/// relative to the resources directory.
const std::wstring ImagesDirectory = L"/images";

/// Directory containing the project levels
/// relative to the resources directory.
const std::wstring LevelsDirectory = L"/levels";

/// Level notices duration in seconds
const double LevelNoticeDuration = 2.0;

/// Size of notices displayed on screen in virtual pixels
const int NoticeSize = 100;

/// Color to draw the level notices
const auto LevelNoticeColor = wxColour(192, 252, 207);


/// Number of all levels
const int NumLevels = 4;


/// SecondsPerMilliseconds
const double SecondsPerMillisecond = 1000.0;

/**
 * Game Constructor
 * @param audioEngine an audio engine
 */
Game::Game(ma_engine * audioEngine) : mAudioEngine(audioEngine) {
    SetImagesDirectory(L".");
    SetLevelsDirectory(L".");
    auto playingArea = new PlayingArea();
    mPlayingArea = playingArea;
    auto audioManager = new AudioManager();
    mAudioManager = audioManager;
}

Game::~Game() {
    ma_engine_uninit(mAudioEngine);
}


/**
 * Add an item to the game
 * @param item New item to add
 */
void Game::Add(std::shared_ptr<Item> item)
{
    mItems.push_back(item);
}


void Game::AddLocation(std::shared_ptr<Item> item, int InitialX, int InitialY)
{
    item->SetLocation(InitialX, InitialY);
    mItems.push_back(item);
}
/**
 * Draw the application (backgorund).
 * @param graphics a pointer for wxGraphicsContext.
 * @param width the width of  the application.
 * @param height the height of the application.
 */
void Game::OnDraw(std::shared_ptr<wxGraphicsContext> graphics,int width, int height)
{
    // Determine the size of the playing area in pixels
    // This is up to you...
    int pixelWidth = mWidth;
    int pixelHeight = mHeight;

    //
    // Automatic Scaling
    //
    auto scaleX = double(width) / double(pixelWidth);
    auto scaleY = double(height) / double(pixelHeight);
    mScale = std::min(scaleX, scaleY);

    mXOffset = (width - pixelWidth * mScale) / 2.0;
    mYOffset = 0;
    if (height > pixelHeight * mScale)
    {
        mYOffset = (double)((height - pixelHeight * mScale) / 2.0);
    }
//######
    graphics->PushState();

    graphics->Translate(mXOffset, mYOffset);
    graphics->Scale(mScale, mScale);

    //
    // Draw in virtual pixels on the graphics context
    //
    // Drawing the background of the game
    //
    wxBrush background(*wxRED);

    graphics->SetBrush(background);
    graphics->DrawRectangle(0, 0, pixelWidth, pixelHeight);

    // Draw all items
    for (const auto& item : mItems) {
        item->Draw(graphics);
    }

        if (mState == GameStates::LevelStart || mState==GameStates::LevelComplete){
        //
        // Measuring text and drawing centered
        //
        wxString noticeText = "Level " + to_string(mCurrentLevel) + (mState == GameStates::LevelStart ? " Begin" : " Completed");
        wxFont bigFont(wxSize(0, NoticeSize),
                       wxFONTFAMILY_SWISS,
                       wxFONTSTYLE_NORMAL,
                       wxFONTWEIGHT_BOLD);
        graphics->SetFont(bigFont, LevelNoticeColor);

        double wid, hit;
        graphics->GetTextExtent(noticeText, &wid, &hit);
        graphics->DrawText(noticeText, mWidth/2 - wid/2, mHeight/2 - hit/2);

        }


    graphics->PopState();
}



std::shared_ptr<Item> Game::HitTest(int x, int y)
{
    for (auto i = mItems.rbegin(); i != mItems.rend();  i++)
    {
        if ((*i)->HitTest(x, y))
        {
            return *i;
        }
    }

    return  nullptr;
}
/**
 * Handle updates for animation
 * @param elapsed The time since the last update
 */
void Game::Update(double elapsed)
{
    // update the second so we can track how many seconds the user has played the level
    // This used for showing the notice text
    mSeconds += elapsed;

    if (mState == GameStates::LevelStart){
        if (mSeconds >= LevelNoticeDuration){
            mState = GameStates::Playing;
            mSeconds = 0;
        }
    }else if (mState == GameStates::LevelFailed){
        if (mSeconds >= LevelNoticeDuration){
            NewGame();
        }
    }else if (mState == GameStates::LevelComplete)
    {
        if(mSeconds >= LevelNoticeDuration)
        {
                if(mCurrentLevel < NumLevels - 1)
                {
                    mCurrentLevel++;
                }else{
                    mCurrentLevel = 0;
                }
                NewGame();
        }
    }else if  (mState == GameStates::Playing)
    {
        if (mPlayingArea != nullptr){
            mPlayingArea->Update(elapsed);
        }
    }
}

void Game::UpdateTracks(double elapsed) {
    for (auto item : mItems){
        item->Update(elapsed);
    }
}
/**
 * Test a key if it has been pressed
 * @param keyCode keycode for the key that has been pressed
*/
void Game::KeyPressed(int keyCode)
{
    KeyVisitor visitor;
    for (const auto& itemPtr : mItems)
    {
        itemPtr->Accept(&visitor);

        auto currentKey = visitor.GetCurrentKey();
        if (currentKey != nullptr)
        {
            if(currentKey->GetKeyCode() == keyCode)
            {
                if (!currentKey->HasKeyPressedBefore(keyCode)){
                    // if the key was pressed for the first time
                    // it will add it to the set and then call the function (KeyDown)
                    currentKey->NewKeyPressed(keyCode);
                    currentKey->KeyDown();
                    break;
                }else{
                    currentKey->KeyHolding();
                    break;
                }
            }
        }
    }

}
/**
 * Test a key if it has been released
 * @param keyCode keycode for the key that has been released
*/
void Game::KeyUp(int keyCode)
{
    KeyVisitor visitor;
    for (const auto& itemPtr : mItems)
    {
        itemPtr->Accept(&visitor);
        auto currentKey = visitor.GetCurrentKey();
        if (currentKey != nullptr)
        {
            if(currentKey->GetKeyCode() == keyCode)
            {
                    // if the key was released it will remove it from the list
                    // and then call the function (KeyUp)
                if (!currentKey->HasKeyPressedBefore(keyCode))
                {
                    currentKey->RemoveKeyPressed(keyCode);
                    currentKey->KeyUp();
                    break;
                }else{
                    currentKey->RemoveKeyPressed(keyCode);
                    currentKey->KeyUpHolding();
                    break;
                }
            }
        }
    }

}


/**
 * Load the level from a file
 * @param filename
 */
void Game::LoadLevel(const wxString &filename)
{
    wxXmlDocument xmlDoc;
    if(!xmlDoc.Load(mLevelsDirectory + "/" + filename))
    {
        wxMessageBox(L"Unable to load Level file");
        return;
    }

    // Get the XML document root node
    auto root = xmlDoc.GetRoot();
    //
    // Traverse the children of the root
    // node of the XML document in memory!!!!
    //
    wxXmlNode* child = root->GetChildren();
    // ** Get the size of the level ** //
    wxString levelSizeValue = root->GetAttribute("size");
    wxStringTokenizer tokenizer(levelSizeValue, ",");
    wxString token;
    // ** resize the window ** //
    if (tokenizer.HasMoreTokens()) {
        token = tokenizer.GetNextToken();
        token.ToInt(&mWidth);
    }
    if (tokenizer.HasMoreTokens()) {
        token = tokenizer.GetNextToken();
        token.ToInt(&mHeight);
    }
    // ** Parsing declarations, music, audio, items ** //
    std::map<wxString, std::shared_ptr<Declarations>> declarationMapPtr = std::map<wxString, std::shared_ptr<Declarations>>();
    // Loop through to find 'each' node

    while (child) {
        if (child->GetName() == "declarations")
        {
            XmlDeclaration(child->GetChildren(),declarationMapPtr);
        }
        else if (child->GetName() == "music")
        {
            XmlMusic(child);
        }
        else if (child->GetName() == "audio")
        {
            XmlAudio(child);
        }
        else if (child->GetName() == "items")
        {
            XmlItem(child->GetChildren(),declarationMapPtr);
        }
        child = child->GetNext();
    }
    // Clear all the items to free memory
    declarationMapPtr.clear();
}

/**
 * Initialize a tag of the declaration in the xml file
 * @param declerationNode a pointer to the item node
 * @param declerationMap a map that contains the declaration and the items
 */
void Game::XmlDeclaration(wxXmlNode* declerationNode, std::map<wxString, std::shared_ptr<Declarations>> &declerationMap){
    for( ; declerationNode; declerationNode=declerationNode->GetNext())
    {
        wxString id = declerationNode->GetAttribute(L"id", L"0").ToStdWstring();
        if(declerationNode->GetName() == "image")
        {
            declerationMap[id] = std::make_unique<DeclarationImage>(declerationNode, this);
        }
        else if(declerationNode->GetName() == "sound-board")
        {
            declerationMap[id] = std::make_unique<DeclerationSoundBoard>(declerationNode, this);
        }
        else if(declerationNode->GetName() == "score-board")
        {
            declerationMap[id] = std::make_unique<DeclerationScoreBoard>(declerationNode, this);
        }
        else if(declerationNode->GetName() == "meter")
        {
            declerationMap[id] = std::make_unique<DeclarationMeter>(declerationNode, this);
        }
        else if(declerationNode->GetName() == "note")
        {
            declerationMap[id] = std::make_unique<DeclarationPuck>(declerationNode, this);
            // ** if we want the pucks to be added to the game we need to uncomment this line, but I think we don't need to ... ** //

           // Add(declerationMap[id]->CreateItem(declerationNode));
        }
    }
}

/**
 * Initialize a tag of the item in the xml file
 * @param itemsNode a pointer to the item node
 * @param declerationMap a map that contains the decleration and the items
 */
void Game::XmlItem(wxXmlNode* itemsNode,  std::map<wxString, std::shared_ptr<Declarations>> &declerationMap){
    // ** add all items each on their own types ** //
    for( ; itemsNode; itemsNode=itemsNode->GetNext()) {
            wxString id = itemsNode->GetAttribute(L"id", L"0").ToStdWstring();
            declerationMap[id]->SetDeclarationMap(declerationMap);
            auto newItem = declerationMap[id]->CreateItem(itemsNode);
            if (newItem != nullptr)
                Add(newItem);
        }
}

/**
 * Initialize a tag of the audio in the xml file
 * @param audioNode a pointer to the audio node
 */
void Game::XmlAudio(wxXmlNode* audioNode){
    auto audio = std::make_unique<DeclarationAudio>(audioNode, this);
    auto audioItem = audio->CreateItem(audioNode->GetChildren());
}

/**
 * Clear all the items in the game
 */
void Game::NewGame(){
    mItems.clear();
    if (mAudioManager != nullptr)
        mAudioManager->Clear();
    mSeconds = 0;
    mState = GameStates::LevelStart;
    LoadLevel("level"+ to_string(mCurrentLevel)+".xml");
    if (mPlayingArea != nullptr)
        mPlayingArea->StartLevel();
}

/**
 * Initialize a tag of the music in the xml file
 * @param musicNode a pointer to the music node
 */
void Game::XmlMusic(wxXmlNode* musicNode){
    auto music = std::make_unique<DeclarationMusic>(musicNode, this);
    auto newMusic = music->CreateItem(musicNode->GetChildren());
//    Add(newMusic);
}

/**
 * Accept a visitor for the collection
 * @param visitor The visitor for the collection
 */
void Game::Accept(ItemVisitor* visitor)
{
    for (auto item : mItems)
    {
        item->Accept(visitor);
    }
}


/**
 * Set the directory the images are stored in
 * @param dir
 */
void Game::SetImagesDirectory(const std::wstring &dir) {
    mImagesDirectory = dir + ImagesDirectory;
}
/**
 * Set the directory the levels are stored in
 * @param dir
 */
void Game::SetLevelsDirectory(const std::wstring &dir) {
    mLevelsDirectory = dir + LevelsDirectory;
}


/**
 * Delete an item
 * @param item
 */
void Game::DeleteItem(std::shared_ptr<Item> item)
{
    if (!item->PendingDelete())
    {
        return;
    }

    auto loc = find(::begin(mItems), ::end(mItems), item);
    if (loc != ::end(mItems))
    {
        mItems.erase(loc);
    }
}