/**
 * @file FindNoteDeclarationVisitor.cpp
 * @author MohammedAlanizy
 */

#include "pch.h"
#include "FindNoteDeclarationVisitor.h"
#include "DeclarationPuck.h"




/**
 * A visitor for a note, it adds the current note to a vector
 * @param puck a pointer to current puck
*/
void FindNoteDeclarationVisitor::VisitPuck(DeclarationPuck* puck){
    if (puck != nullptr)
        mPucks.push_back(std::shared_ptr<DeclarationPuck>(puck));
}