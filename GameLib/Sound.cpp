/**
 * @file Sound.cpp
 * @author Abdulrahman Alanazi
 */


#include "pch.h"

#include "Sound.h"
#include "Item.h"
#include <wx/string.h>
#include <wx/msgdlg.h>
#include <stdexcept>

/// Directory that contains the audio files
const std::wstring AudioDirectory = L"audio";

Sound::Sound(Game * game,std::wstring filename, ma_engine* audioEngine, bool isLong,double volume) : Item(game), mLong(isLong), mVolume(volume) {
    if (audioEngine != nullptr) {
        auto audioFile = AudioDirectory + L"/" + filename;
        auto result = ma_sound_init_from_file(audioEngine, wxString(audioFile), 0, NULL, NULL, &mSound);
        if (result != MA_SUCCESS) {
            // Handle audio loading error
            throw std::runtime_error("Failed to load audio file: " + wxString(audioFile));
        } else {
            mLoaded = true;
            ma_sound_set_volume(&mSound, mVolume);
        }
    }
}

/**
 * Destructor
 */
Sound::~Sound()
{
    if(mLoaded)
    {
        if(ma_sound_is_playing(&mSound))
        {
            ma_sound_stop(&mSound);
        }

        ma_sound_uninit(&mSound);
    }
}

/**
 * Play the sound
 */
void Sound::PlaySound()
{
    if(mLoaded)
    {
        // If the sound is already playing, stop it first
        if(ma_sound_is_playing(&mSound))
        {
            ma_sound_stop(&mSound);
        }

        // Always rewind to the beginning before playing
        ma_sound_seek_to_pcm_frame(&mSound, 0);

        // And play the sound!
        ma_sound_start(&mSound);
    }
}

/**
 * Stop playing the sound
 */
void Sound::PlayEnd()
{
    if(mLoaded)
    {
        // If the sound is already playing, stop it first
        if(ma_sound_is_playing(&mSound))
        {
            ma_sound_stop(&mSound);
        }
    }
}