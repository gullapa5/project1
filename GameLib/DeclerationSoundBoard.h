/**
 * @file DeclerationSoundBoard.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLERATIONSOUNDBOARD_H
#define PROJECT1_GAMELIB_DECLERATIONSOUNDBOARD_H

#include "Declarations.h"
#include "SoundBoard.h"
/**
 * handling the soundboard declaration tag in the xml file
 */
class DeclerationSoundBoard : public Declarations
{
private:

public:
    DeclerationSoundBoard(wxXmlNode *node,Game* game);

     void CreateTracks(std::shared_ptr<SoundBoard>, wxXmlNode *node);
     void CreatePucks(std::shared_ptr<SoundBoard> soundBoard);
     std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;
    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    void Accept(DeclarationVisitor* visitor) override { };
};

#endif //PROJECT1_GAMELIB_DECLERATIONSOUNDBOARD_H
