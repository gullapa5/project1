/**
 * @file SoundBoardFinderVisitor.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SOUNDBOARDFINDERVISITOR_H
#define PROJECT1_GAMELIB_SOUNDBOARDFINDERVISITOR_H

#include "ItemVisitor.h"
/**
 * Visitor class for the soundboard
 */
class SoundBoardFinderVisitor : public ItemVisitor
{
private:
    /// A pointer to the current soundBoard of a visitor.
    SoundBoard* mSoundBoard;
public:
    /// Initialize mSoundBoard with a null pointer
    SoundBoardFinderVisitor() : mSoundBoard(nullptr) {}
    void VisitSoundBoard(SoundBoard* soundBoard);
    /**
     *  A function to return the current soundBoard.
     * @return soundBoard; the current soundBoard.
     */
    SoundBoard* GetSoundBoard() {return mSoundBoard;};

};

#endif //PROJECT1_GAMELIB_SOUNDBOARDFINDERVISITOR_H
