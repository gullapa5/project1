/**
 * @file MainFrame.h
 * @author Abdulrahman Alanazi
*/

#ifndef PROJECT1__MAINFRAME_H
#define PROJECT1__MAINFRAME_H
#include "GameView.h"
/**
 * The top-level (main) frame of the application
 */

class MainFrame : public wxFrame {
private:
    GameView * mGameView; ///< Game view
public:
    void Initialize(ma_engine *audioEngine);
    void OnExit(wxCommandEvent &event);
    void LoadLevelOption(wxCommandEvent &event);
    void OnAbout(wxCommandEvent &event);
};



#endif //PROJECT1__MAINFRAME_H
