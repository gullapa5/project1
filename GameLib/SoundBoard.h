/**
 * @file SoundBoard.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__SOUNDBOARD_H
#define PROJECT1__SOUNDBOARD_H

#include "Item.h"
#include "Note.h"
#include "Puck.h"
#include "Sound.h"
#include "Image.h"

class Track;
/**
 * SoundBoard class that manage the functionality and the rendering of the SoundBoard
 */
class SoundBoard : public Item{

private:
    std::map<wxString, std::shared_ptr<Puck>> mPucks; ///< Map containing pucks associated with their IDs.
    std::vector<std::shared_ptr<Track>> mTracks;      ///< Vector containing tracks.
    int mTopWidth;                                     ///< Width of the top area.
    int mSizeBeats;                                    ///< Size of beats.
    std::shared_ptr<Item> mCoverSoundBoard;           ///< Pointer to the cover soundboard item.
public:
    /// Copy constructor (disabled)
    SoundBoard(const SoundBoard &) = delete;

    /// Assignment operator
    void operator=(const SoundBoard &) = delete;
    /**
     * It adds a track to a vector of tracks
     * @param track a shared pointer to the track
     */
    void AddTrack(std::shared_ptr<Track> track){mTracks.push_back(track);};

    std::shared_ptr<Track> GetTrack(wxString id);
    /**
     * It adds a puck to a map using its id
     * @param id id of the puck
     * @param puck a shared pointer to the puck
     */
    void AddPuck(wxString id , std::shared_ptr<Puck> puck){mPucks[id] = puck;};
    /**
     * It gets a puck using its id
     * @param id id of the puck
     * @return get puck
     */
    std::shared_ptr<Puck> GetPuck(wxString id){return mPucks[id] ;};
    /**
     * Set top width
     * @param topWidth int of the top width
     */
    void SetTopWidth(int topWidth){mTopWidth=topWidth;};
    /**
     * Get top width
     * @return int of the top width
     */
    int GetTopWidth(){return mTopWidth;};
    /**
    * Get Cover Image
     * @return cover image
    */
    std::shared_ptr<Item> GetCoverImage() { return mCoverSoundBoard;};
    /**
     * Set cover image
     * @param image image of the cover
     */
    void SetCoverImage(std::shared_ptr<Image> image) { mCoverSoundBoard = image;};
    SoundBoard(Game *game);
    /**
     * Accept a visitor for soundBoard
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) override {visitor->VisitSoundBoard(this);};
    /**
     * Set size beats
     * @param sizeBeats int of the size beats
     */
    void SetSizeBeats(int sizeBeats){mSizeBeats=sizeBeats;};
    /**
     * Get tracks
     * @return tracks
     */
    std::vector<std::shared_ptr<Track>> GetTracks(){return mTracks;};

    void Update(double elapsed) override;
    /**
     * Get size beats
     * @return int of the size beats
     */
    int GetSizeBeats(){return mSizeBeats;};
};

#endif //PROJECT1__SOUNDBOARD_H
