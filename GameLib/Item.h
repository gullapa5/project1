/**
 * @file Item.h
 * @author kraze
 *
 *
 */

#ifndef PROJECT1__ITEM_H
#define PROJECT1__ITEM_H

#include <wx/wx.h>
#include "ItemVisitor.h"
#include <map>
#include <wx/xml/xml.h>
class Game;
/**
 * Base class for any item in our Game.
 */
class Item{

protected:
    /**
* Constructor
* @param game Pointer to the Game instance associated with the item.
*/
    Item(Game* game);

/**
 * Constructor
 * @param game Pointer to the Game instance associated with the item.
 * @param filename The filename to initialize the item with.
 */
    Item(Game* game, const std::wstring& filename);


private:
    /// The game this item is contained in
    Game   *mGame;

    // Item location in the game
    double  mX = 0;     ///< X location for the center of the item
    double  mY = 0;     ///< Y location for the center of the item

    int mWidth = 0;     ///< Width of the item
    int mHeight = 0;    ///< Height of the item

    wxString mId; ///< ID of the item
    wxString mImageName;///< image file name  of the item
    /// The underlying item image
    std::unique_ptr<wxImage> mItemImage;

    /// The bitmap we can display for this item
    std::unique_ptr<wxBitmap> mItemBitmap;

public:
    virtual ~Item();

    /**
     * The X location of the item
     * @return X location in pixels
     */
    double GetX() const { return mX; }

    /**
     * Get the image
     * @return a unique pointer to the image
     */
    std::unique_ptr<wxBitmap>& GetImage(){return mItemBitmap;};


    /**
     * The Y location of the item
     * @return Y location in pixels
     */
    double GetY() const { return mY; }

    /**
     * The Width of the item
     * @return Width  in pixels
     */
    double GetWidth() const { return mWidth; }

    /**
     * The height of the item
     * @return height  in pixels
     */
    double GetHeight() const { return mHeight; }
    /**
     * The image name of the item
     * @return wxString filename
     */
    wxString GetImageName() const { return mImageName; }
    /**
     * Set the item location
     * @param x X location in pixels
     * @param y Y location in pixels
     */
    void SetLocation(double x, double y) { mX = x; mY = y; }

    /// Default constructor (disabled)
    Item() = delete;

    /// Copy constructor (disabled)
    Item(const Item &) = delete;

    /// Assignment operator
    void operator=(const Item &) = delete;

    /**
    * Draws the items
    * @param graphics A shared pointer to the wxGraphicsContext on which the items will be drawn
    */
    virtual void Draw(std::shared_ptr<wxGraphicsContext> graphics);
    /**
     * Handle updates for animation
     * @param elapsed The time since the last update
     */
    virtual void Update(double elapsed) {}

    /**
     * Gets the current game
     * @return The current game
     */
    Game* GetGame() const{return mGame;}

    /**
     * Gets the ID
     * @return The ID
     */
    wxString GetId(){return mId;};

    /**
     * Sets the current game
     * @param id The id
     */
    void SetId(wxString id){mId = id;};
    /**
     * Sets the image
     * @param imageName The image name
     */
    virtual void SetImage(const std::wstring &imageName);
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) = 0;
    /**
     * Resizes the Image
     * @param width The image width
     * @param height The image height
     */
    void ResizeImage(int width, int height);


    /**
     * Indicate that this object is about to be deleted by
     * begin dragged into the trash can. If the function
     * override returns false, the delete will not occur.
     * @return true if okay to delete.
    */
    virtual bool PendingDelete() { return true; }

    /**
     * Sees if a item was hit
     * @param x The x coordinate of the test
     * @param y The y coordinate of the test
     * @return true if it hits an item, false if it does not
     */
    bool HitTest(int x, int y);
};

#endif //PROJECT1__ITEM_H