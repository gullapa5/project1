/**
 * @file Puck.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__PUCK_H
#define PROJECT1__PUCK_H

#include "Item.h"
/**
 * Puck class that manage the functionality and the rendering of the Pucks
 */
class Puck : public Item {

private:
    int mTrack; ///<track of the puck
    double mTolerance; ///<tolerance of the puck
    int mWidth; ///<width of the puck
    int mHeight; ///<Height of the puck
    bool mShow = false; ///< It uses to show the pucks
public:
    /// Default constructor (disabled)
    Puck() = delete;

    /// Copy constructor (disabled)
    Puck(const Puck &) = delete;

    /// Assignment operator
    void operator=(const Puck &) = delete;
    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(ItemVisitor* visitor) override {visitor->VisitPuck(this);};
    /**
    * Get a track number of a puck
    * @return Track number
    */
    int GetTrackNumber(){return mTrack;};
    /**
    * Get a tolerance number of a puck
    * @return tolerance number
    */
    double GetTolerance(){return mTolerance;};
    /**
    * Get base width
    * @return base width
    */
    int GetBaseWidth(){return mWidth;};
    /**
    * Get base height
    * @return base width
    */
    int GetBaseHeight(){return mHeight;};
    /**
    * return if the puck should be shown or not
    * @param show
    */
    void Show(bool show){mShow = show;};
    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;


    Puck(Game *game,int width,int height,int track,double tolerance);

};

#endif //PROJECT1__PUCK_H
