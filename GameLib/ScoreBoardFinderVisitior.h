/**
 * @file ScoreBoardFinderVisitior.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SCOREBOARDFINDERVISITIOR_H
#define PROJECT1_GAMELIB_SCOREBOARDFINDERVISITIOR_H

#include "ItemVisitor.h"

/**
 * Visitor class for scoreboard finder class
 */
class ScoreBoardFinderVisitior : public ItemVisitor
{
private:
    /// A pointer to the current ScoreBoard of a visitor.
    ScoreBoard* mScoreBoard;
public:
    /// Initialize mScoreBoard with a null pointer
    ScoreBoardFinderVisitior() : mScoreBoard(nullptr) {}
    void VisitScoreBoard(ScoreBoard* scoreBoard);
    /**
     *  A function to return the current ScoreBoard.
     * @return ScoreBoard; the current ScoreBoard.
     */
    ScoreBoard* GetScoreBoard() {return mScoreBoard;};

};

#endif //PROJECT1_GAMELIB_SCOREBOARDFINDERVISITIOR_H
