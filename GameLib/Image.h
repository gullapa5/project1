/**
 * @file Image.h
 * @author Abdulrahman Alanazi
 *
 *
 */

#ifndef PROJECT1_GAMELIB_IMAGE_H
#define PROJECT1_GAMELIB_IMAGE_H

#include "Item.h"
/**
 * handles all the images of the items
 */
class Image : public Item
{
private:


public:
    /// Default constructor (disabled)
    Image() = delete;

    /// Copy constructor (disabled)
    Image(const Image &) = delete;

    /// Assignment operator
    void operator=(const Image &) = delete;
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) {};
    /// Default constructor
    Image(Game *game);



};

#endif //PROJECT1_GAMELIB_IMAGE_H
