/**
 * @file DeclarationMeter.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONMETER_H
#define PROJECT1_GAMELIB_DECLARATIONMETER_H
#include "Declarations.h"
/**
 * handling the meter declaration tag in the xml file
 */
class DeclarationMeter : public Declarations
{
private:

public:
    DeclarationMeter(wxXmlNode *node,Game* game);

    std::shared_ptr<Item> CreateItem(wxXmlNode *node) override;
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(DeclarationVisitor* visitor) override {};
};


#endif //PROJECT1_GAMELIB_DECLARATIONMETER_H
