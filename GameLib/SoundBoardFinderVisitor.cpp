/**
 * @file SoundBoardFinderVisitor.cpp
 * @author MohammedAlanizy
 */
#include "pch.h"
#include "SoundBoardFinderVisitor.h"

/**
 * Assign the member variable to the soundBoard object
 * @param soundBoard soundBoard object to assign it.
*/
void SoundBoardFinderVisitor::VisitSoundBoard(SoundBoard *soundBoard) {
    if (soundBoard != nullptr)
    {
        mSoundBoard = soundBoard;
    }
}