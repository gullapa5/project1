/**
 * @file FindNoteDeclarationVisitor.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_FINDNOTEDECLARATIONVISITOR_H
#define PROJECT1_GAMELIB_FINDNOTEDECLARATIONVISITOR_H

#include "DeclarationVisitor.h"
#include <wx/xml/xml.h>
/**
 * It finds all notes in the declaration vistor
 */
class FindNoteDeclarationVisitor : public DeclarationVisitor
{
    /// A pointer to the all notes of a visitor.
    std::vector<std::shared_ptr<DeclarationPuck>> mPucks;
public:
    /// Initialize mPucks with a null
    FindNoteDeclarationVisitor() : mPucks() {}
     void VisitPuck(DeclarationPuck* puck);
    /**
     * get all pucks declaration.
     * @return a vector of all pucks.
    */
    std::vector<std::shared_ptr<DeclarationPuck>> GetAllPucks(){return mPucks;};
};

#endif //PROJECT1_GAMELIB_FINDNOTEDECLARATIONVISITOR_H
