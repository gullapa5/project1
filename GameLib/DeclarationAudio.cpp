/**
 * @file DeclarationAudio.cpp
 * @author MohammedAlanizy
 *
 *
 */

#include "pch.h"
#include "DeclarationAudio.h"
#include "Sound.h"
#include "AudioManager.h"
#include "Game.h"

/**
 * DeclarationAudio Constructor
 */
DeclarationAudio::DeclarationAudio(wxXmlNode *node,Game* game) : Declarations(node,game){

}

/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclarationAudio::CreateItem(wxXmlNode *node){
    for( ; node; node=node->GetNext()) {
        std::wstring name = node->GetAttribute(L"name").ToStdWstring();
        std::wstring audioName = node->GetAttribute(L"audio").ToStdWstring();
        std::wstring isLong = node->GetAttribute(L"long", L"false").ToStdWstring();
        double volume;
        node->GetAttribute(L"volume", L"1.0").ToDouble(&volume);
        // ** Initialization ** //
        auto newSound = std::make_shared<Sound>(GetGame(), audioName, GetGame()->GetAudioEngine(), !(isLong == "false"),volume);
        GetGame()->GetAudioManager()->AddAudio(name,newSound);
    }
    return nullptr;
}