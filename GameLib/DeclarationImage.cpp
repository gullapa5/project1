/**
 * @file DeclarationImage.cpp
 * @author MohammedAlanizy
 */


#include "pch.h"
#include "DeclarationImage.h"
#include "Item.h"
#include "Game.h"
#include "Image.h"

/**
 * DeclarationImage Constructor
  * @param node a pointer to the XmlNode
 * @param game a pointer to the game
 */
DeclarationImage::DeclarationImage(wxXmlNode *node,Game* game) : Declarations(node,game){

}

/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclarationImage::CreateItem(wxXmlNode *node){
    // ** Declaration ** //
    std::wstring imageName =  GetDeclaration()->GetAttribute(L"image").ToStdWstring();
    std::wstring imageSizeWithComma =  GetDeclaration()->GetAttribute(L"size", L"0,0").ToStdWstring();
    wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
    // ** Item ** //
    std::wstring imagePositionWithComma =  node->GetAttribute(L"p", L"0,0").ToStdWstring();
    wxSize imagePosition = GetNumberBetweenComma(imagePositionWithComma);
    // ** Initialization ** //
    auto newImage = std::make_shared<Image>(GetGame());
    newImage->SetImage(imageName);
    newImage->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newImage->SetLocation(imagePosition.GetX(),imagePosition.GetY());
    return newImage;
}