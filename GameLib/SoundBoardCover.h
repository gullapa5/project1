/**
 * @file SoundBoardCover.h
 * @author kraze
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SOUNDBOARDCOVER_H
#define PROJECT1_GAMELIB_SOUNDBOARDCOVER_H

#include "SoundBoard.h"
/**
 * Sound board cover class that updates with score
 */
class SoundBoardCover : public SoundBoard{

private:
        /** Constructor
     * @param game The game we are iterating over
     * @param filename name of file
     */
    SoundBoardCover(Game *game, const std::wstring &filename);
public:
    // Default constructor (disabled)
    SoundBoardCover() = delete;

    /// Copy constructor (disabled)
    SoundBoardCover(const SoundBoardCover &) = delete;

    /// Assignment operator
    void operator=(const SoundBoardCover &) = delete;

    SoundBoardCover(Game* game);
    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(ItemVisitor* visitor) {};
};

#endif //PROJECT1_GAMELIB_SOUNDBOARDCOVER_H
