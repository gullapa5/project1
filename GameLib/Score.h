/**
 * @file Score.h
 * @author Austin Kim
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SCORE_H
#define PROJECT1_GAMELIB_SCORE_H

#include "Item.h"

/**
 * handling the score of the game
 */
class Score: public Item
{
protected:
    Score(Game *game);

private:


public:
    /// Default constructor (disabled)
    Score() = delete;

    /// Copy constructor (disabled)
    Score(const Score &) = delete;

    /// Assignment operator
    void operator=(const Score &) = delete;

};

#endif //PROJECT1_GAMELIB_SCORE_H
