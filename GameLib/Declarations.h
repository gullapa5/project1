/**
 * @file Declarations.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_DECLARATIONS_H
#define PROJECT1_GAMELIB_DECLARATIONS_H

#include "Item.h"
#include "Note.h"
#include "DeclarationVisitor.h"
/**
 * handling all declaration tag in the xml file
 */
class Declarations
{
private:
    /// A pointer of declaration
    wxXmlNode *mDeclaration;
    /// Current game
    Game *mGame;
    /// A map that has declaration
    std::map<wxString, std::shared_ptr<Declarations>>  mDeclerationMap;
public:
    Declarations(wxXmlNode *node, Game *game);
    /// Deconstruct
    virtual ~Declarations() = default;
    /// Copy constructor (disabled)
    Declarations(const Declarations &) = delete;

    /// Assignment operator
    void operator=(const Declarations &) = delete;
    /**
     * get a node of declaration.
     * @return a pointer to wxXmlNode.
    */
    wxXmlNode *GetDeclaration() {return mDeclaration;};
        /**
     * Creates the item
     * @param node node to the xml file
     * @return the shared ptr to the new created item
     */
    virtual std::shared_ptr<Item> CreateItem(wxXmlNode *node) = 0;
    wxSize GetNumberBetweenComma(wxString targetString);

    /**
     * get all declaration.
     * @return a map that has declaration.
    */
    std::map<wxString, std::shared_ptr<Declarations>> GetDeclarationsItems() {return mDeclerationMap;};

    /**
     * Set all declaration.
     * @param declarationMap a map that has declaration
    */
    void SetDeclarationMap(std::map<wxString, std::shared_ptr<Declarations>> declarationMap){mDeclerationMap = declarationMap;};
    /**
     * Get the game
     * @return A pointer to the game that we are working on it.
    */
    Game *GetGame() {return mGame;}

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    virtual void Accept(DeclarationVisitor* visitor) = 0;

};

#endif //PROJECT1_GAMELIB_DECLARATIONS_H
