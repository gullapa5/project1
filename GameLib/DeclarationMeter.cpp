/**
 * @file DeclarationMeter.cpp
 * @author MohammedAlanizy
 */

#include "pch.h"
#include "DeclarationMeter.h"
#include "Meter.h"
#include "Needle.h"
#include "Game.h"
#include "Image.h"

/**
 * DeclarationMeter Constructor
  * @param node a pointer to the XmlNode
 * @param game a pointer to game
 */
DeclarationMeter::DeclarationMeter(wxXmlNode *node,Game* game) : Declarations(node,game){

}



/**
 * Create the item for the target declaration
 * @param node a pointer to the XmlNode
 * @return an item that has been created.
*/
std::shared_ptr<Item> DeclarationMeter::CreateItem(wxXmlNode *node){
    // ** Declaration ** //
    std::wstring imageName =  GetDeclaration()->GetAttribute(L"image", L"images/not-found.png").ToStdWstring();
    std::wstring imageNeedle =  GetDeclaration()->GetAttribute(L"needle", L"images/not-found.png").ToStdWstring();
    std::wstring imageCover =  GetDeclaration()->GetAttribute(L"cover", L"images/not-found.png").ToStdWstring();
    std::wstring imageSizeWithComma =  GetDeclaration()->GetAttribute(L"size", L"0,0").ToStdWstring();
    wxSize imageSize = GetNumberBetweenComma(imageSizeWithComma);
    // ** Item ** //
    std::wstring meterPositionWithComma =  node->GetAttribute(L"p", L"0,0").ToStdWstring();
    wxSize meterPosition = GetNumberBetweenComma(meterPositionWithComma);
    // ** Initialization ** //

    // ** Meter ** //
    auto newMeter = std::make_shared<Meter>(GetGame());
    newMeter->SetImage(imageName);
    newMeter->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newMeter->SetLocation(meterPosition.GetX(),meterPosition.GetY());

    // We will override this as the meter needs to be drawn first
    GetGame()->Add(newMeter);

    // ** Meter Needle ** //
    auto newNeedle = std::make_shared<Needle>(GetGame());
    newNeedle->SetImage( imageNeedle);
    newNeedle->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newNeedle->SetLocation(meterPosition.GetX(),meterPosition.GetY());

    // We will override this as the needle needs to be drawn second
    GetGame()->Add(newNeedle);

    // ** Meter Cover** //
    auto newCover = std::make_shared<Image>(GetGame() );
    newCover->SetImage( imageCover);
    newCover->ResizeImage(imageSize.GetWidth(),imageSize.GetHeight());
    newCover->SetLocation(meterPosition.GetX(),meterPosition.GetY());
    return newCover;
}