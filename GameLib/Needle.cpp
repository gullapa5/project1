/**
 * @file Needle.cpp
 * @author MohammedAlanizy
 */

#include "pch.h"
#include "Needle.h"
#include "Game.h"
#include "PlayingArea.h"
#include <wx/graphics.h>

/// Positive or negative rotations of this amount will move
/// the needle to the limit in that direction.
/// A meter score of 0 will be a rotation of -0.9
/// A meter score of 100% (11) will be a rotation of 0.9
const double MaxNeedleRotation = 0.9;

/// This is how far down the need image the pivot point is
/// as a percentage of the height of the image.
const double NeedlePivotYOffset = 0.80;



/**
 * Constructor
 * @param game Game
 */
Needle::Needle(Game *game) : Meter(game)
{

}
/**
 * Draw a needle
 * @param graphics the graphics
 */
void Needle::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{

    int wid = GetWidth();
    int hit = GetHeight();
    int needlePivotY = (int)(hit * NeedlePivotYOffset);

    graphics->PushState();
    graphics->Translate(GetX(), GetY() + needlePivotY - hit/2);

    // Calculate the range of rotation
    double rotationRange = MaxNeedleRotation - (-MaxNeedleRotation);

    double normalizedScore;


    if (GetGame()->GetPlayingArea()->GetAllNotesCount() <= GetGame()->GetPlayingArea()->GetBeatsPerMeasure()  ){
        // if the game was within the two seconds then it will set it as the max score.
        normalizedScore = 1.0;
    }else {
        // Calculate the normalized meter score (between 0 and 1)
        normalizedScore = (GetGame()->GetPlayingArea()->GetCorrectNote() )/ (GetGame()->GetPlayingArea()
            ->GetAllNotesCount());
    }
    if (normalizedScore > 1.0){
        normalizedScore = 1.0;
    }
    // Calculate the rotation based on the meter score
    double rotation = (-MaxNeedleRotation) + (rotationRange * normalizedScore);
    graphics->Rotate(rotation);
    graphics->DrawBitmap(*GetImage(),
                         -wid/2,
                         -needlePivotY,
                         wid, hit);

    graphics->PopState();

}