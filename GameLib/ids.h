/**
 * @file ids.h
 * @author Austin Kim
 *
 * ID values for our program.
 */

#ifndef PROJECT1__IDS_H
#define PROJECT1__IDS_H
/**
*All the IDs
 */

enum IDs {
    /// Level>Level0 menu option
    IDM_Level0 = wxID_HIGHEST ,
    /// Level>Level1 menu option
    IDM_Level1 = wxID_HIGHEST + 1,
    /// Level>Level2 menu option
    IDM_Level2 = wxID_HIGHEST + 2,
    /// Level>Level3 menu option
    IDM_Level3 = wxID_HIGHEST + 3,
    /// Level>Auto Play menu option
    IDM_AUTOPLAY = wxID_HIGHEST + 4,
};

#endif //PROJECT1__IDS_H
