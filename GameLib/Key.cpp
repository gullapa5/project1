/**
 * @file Key.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "Key.h"
#include "Puck.h"
#include "SoundBoardFinderVisitor.h"
#include <string>
#include "SoundBoard.h"
#include "Track.h"
#include "ScoreBoard.h"
#include "PlayingArea.h"
#include "ScoreBoardFinderVisitior.h"

/// Offset of y when a key is preesed
const int OffsetClick = 5;



// Constructor to set the
Key::Key(Game* game,int key): Item(game) , mKey(key){

}

/**
 * Find a key in a set to determine if it was pressed before or not.
 * @param keyCode the keyCode that we are looking for
 * @return a boolean; true if the key has been pressed before.
 */
bool Key::HasKeyPressedBefore(int keyCode){
    if(mKeys.find(keyCode) != mKeys.end())
        return true;

    // if the key wasn't found
    return false;
}


/**
 * Handle a key press function
 */
void Key::KeyDown()
{
    if (mAnimation){
        SetLocation(GetX(), GetY()+OffsetClick);
    }
    if (mHighlights)
    {
    SetImage(L"HighlightedImages/"+ GetImageName().ToStdWstring());
    }
    SoundBoardFinderVisitor visitor;
    GetGame()->Accept(&visitor);
    auto soundBoard = visitor.GetSoundBoard();
    for (auto note : soundBoard->GetTrack(GetId())->GetNotes()){
        if (!note->DidScoreTaken())
        {
            double absoluteBeat = (note->GetMeasure() - 1.0) * GetGame()->GetPlayingArea()->GetBeatsPerMeasure()
                + (note->GetBeat() - 1.0);
            double distance = absoluteBeat - GetGame()->GetPlayingArea()->GetCurrentBeat();
            if(( distance > 0
                && distance<= soundBoard->GetSizeBeats()
                && absoluteBeat - GetGame()->GetPlayingArea()->GetCurrentBeat() <= note->GetTolerance()))
            {
                // we found the current note
                mHoldingTime = GetGame()->GetPlayingArea()->GetCurrentBeat();
                mCurrentPlaying = note;
                GetGame()->GetPlayingArea()->IncrementScore();
                GetGame()->GetAudioManager()->GetAudio(note->GetSoundName())->PlaySound();
                note->TakeScore();
                break;
            }
        }
    }
}
/**
 * Handle a key holding function
 */
void Key::KeyHolding()
{
//    std::cout << " holding a key !" << std::endl;

}
/**
 * Handle a key up function
 */
void Key::KeyUp()
{
    if (mAnimation) {
        SetLocation(GetX(), GetY()-OffsetClick);
    }
    if (mHighlights)
    {
        std::wstring image = GetImageName().ToStdWstring();
        size_t pos = image.find(L"HighlightedImages/");
        if(pos != std::wstring::npos)
        {
            image.replace(pos, std::wstring(L"HighlightedImages/").length(), L"");
        }
        SetImage(image);
    }
    if (mCurrentPlaying != nullptr){
        bool isLong = (GetGame()->GetAudioManager()->GetAudio(mCurrentPlaying->GetSoundName())->GetIsLong());
        if (isLong)
        {
            GetGame()->GetAudioManager()->GetAudio(mCurrentPlaying->GetSoundName())->PlayEnd();
            double absoluteBeat = (mCurrentPlaying->GetMeasure() - 1.0) * GetGame()->GetPlayingArea()->GetBeatsPerMeasure();
            double timeForHolding = GetGame()->GetPlayingArea()->GetCurrentBeat() - mHoldingTime;
            double distance = (absoluteBeat + mCurrentPlaying->GetDuration()) - GetGame()->GetPlayingArea()->GetCurrentBeat();
            if(timeForHolding <= mCurrentPlaying->GetDuration() )
            {
                // the user holding the key correctly
                mCurrentPlaying->TakeBouns();
                GetGame()->GetPlayingArea()->BonusScore(timeForHolding, mCurrentPlaying->GetDuration());
            }
        }

    }

}

void Key::KeyUpHolding()
{
    KeyUp();
//    SoundBoardFinderVisitor visitor;
//    GetGame()->Accept(&visitor);
//    auto soundBoard = visitor.GetSoundBoard();
//    if (mCurrentPlaying != nullptr)
//    {
//        if(!mCurrentPlaying->DidBounsTaken())
//        {
//                + (mCurrentPlaying->GetBeat() - 1.0);
//            bool isLong = false;
//            if(GetGame()->GetAudioManager()->GetAudio(mCurrentPlaying->GetSoundName()) != nullptr)
//            { // we check just in case ...
//                isLong = GetGame()->GetAudioManager()->GetAudio(mCurrentPlaying->GetSoundName())->GetIsLong();
//            }
//            if(isLong)
//            {
//                double distance = (absoluteBeat + mCurrentPlaying->GetDuration()) - GetGame()->GetPlayingArea()->GetCurrentBeat();
//                if(timeForHolding <= mCurrentPlaying->GetDuration() && distance > 0)
//                {
//                    // the user holding the key correctly
//                    mCurrentPlaying->TakeBouns();
//                    GetGame()->GetPlayingArea()->BonusScore(timeForHolding, mCurrentPlaying->GetDuration());
//                }
//            }
//        }
//    }
}