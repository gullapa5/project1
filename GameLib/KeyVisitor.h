/**
 * @file KeyVisitor.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_KEYVISITOR_H
#define PROJECT1_GAMELIB_KEYVISITOR_H

#include "ItemVisitor.h"
#include "Key.h"
/**
 * A vistor class for any key pressed
 */
class KeyVisitor : public ItemVisitor
{
private:
    /// A pointer to the current key of a visitor.
    Key* mKey;
public:
    /// Initialize mKey with a null pointer
    KeyVisitor() : mKey(nullptr) {}
    void VisitKey(Key* key) override;

    /**
     *  A function to return the current key.
     * @return Key; the current key.
     */
    Key* GetCurrentKey() {return mKey;};
};

#endif //PROJECT1_GAMELIB_KEYVISITOR_H
