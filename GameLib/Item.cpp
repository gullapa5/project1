/**
 * @file Item.cpp
 * @author kraze
 */

#include "pch.h"
#include "Item.h"
#include "Game.h"
#include <wx/graphics.h>
#include <wx/tokenzr.h>

using namespace std;

/**
 * Destructor
 */
Item::~Item()
{

}


void Item::ResizeImage(int width, int height)
{
        mWidth = width;
        mHeight = height;
}


Item::Item(Game *game) : mGame(game)
{

}


 void Item::SetImage(const std::wstring &imageName) {
    if (!imageName.empty())
    {
        wstring filename = mGame->GetImagesDirectory() + L"/" + imageName;
        mImageName = imageName;
        mItemImage = make_unique<wxImage>(filename, wxBITMAP_TYPE_ANY);
        mItemBitmap = make_unique<wxBitmap>(*mItemImage);
    }
    else
    {
        mImageName = "";
        mItemImage.release();
        mItemBitmap.release();
    }
 }

void Item::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    if (mItemImage != nullptr)
    {
        graphics->DrawBitmap(*mItemBitmap, int(GetX() - mWidth / 2), int(GetY() - mHeight / 2), mWidth, mHeight);
    }
}


bool Item::HitTest(int x, int y)
{
    double wid = mItemBitmap->GetWidth();
    double hit = mItemBitmap->GetHeight();

    /// Make x and y relative to the top-left corner of the bitmap image
    /// Subtracting the center makes x, y relative to the image center
    /// Adding half the size makes x, y relative to theimage top corner
    double testX = x - GetX() + wid / 2;
    double testY = y - GetY() + hit / 2;

    /// Test to see if x, y are in the image
    if (testX < 0 || testY < 0 || testX >= wid || testY >= hit)
    {
        /// We are outside the image
        return false;
    }

    /// Test to see if x, y are in the drawn part of the image
    /// If the location is transparent, we are not in the drawn
    /// part of the image
    return !mItemImage->IsTransparent((int)testX, (int)testY);
}

Item::Item(Game *game, const std::wstring &filename) : mGame(game)
{
    mItemImage = make_unique<wxImage>(filename, wxBITMAP_TYPE_ANY);
    mItemBitmap = make_unique<wxBitmap>(*mItemImage);
}
