/**
 * @file Note.h
 * @author MohammedAlanizy
 *
 *
 */

#ifndef PROJECT1_GAMELIB_NOTE_H
#define PROJECT1_GAMELIB_NOTE_H

#include "Puck.h"

/**
 * handling the notes for the pucks
 */
class Note : public Puck
{
private:
    wxString mSoundName;  ///< SoundName
    int mMeasure;  ///< Measure of note
    double mBeat;///< beat of note
    double mDuration; ///< duration of note
    bool mBounsTaken = false; ///< whether of bonus taken or not
    bool mScoreTaken = false; ///< whether of score taken or not
public:
    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) {};
    /**
     * Gets a beat
     * @return current beat of the note
     */
    double GetBeat(){return mBeat;};
    /**
     * Gets a measure
     * @return current measure of the note
     */
    double GetMeasure(){return mMeasure;};
    /**
     * Gets a duration
     * @return current duration of the note
     */
    double GetDuration(){return mDuration;};
    /**
     * Gets whether if bound taken or not
     * @return boolean of the note
     */
    bool DidBounsTaken(){return mBounsTaken;};
    /**
     * Gets whether if socre is taken or not
     * @return boolean if the score taken true.
     */
    bool DidScoreTaken(){return mScoreTaken;};
    /**
     * It changes mBounsTake to true
     */
    void TakeBouns(){mBounsTaken = true;};
    /**
     * It changes mScoreTaken to true
     */
    void TakeScore(){mScoreTaken = true;};
    /**
     * Gets a sound name
     * @return sound name of the note.
     */
    wxString GetSoundName(){return mSoundName;};
    Note(Game *game,int width, int height,wxString soundName,double beat, double duration,int measure,int track, double tolerance);
};

#endif //PROJECT1_GAMELIB_NOTE_H
