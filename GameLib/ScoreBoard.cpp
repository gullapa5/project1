/**
 * @file ScoreBoard.cpp
 * @author Abdulrahman Alanazi
 */
#include "pch.h"
#include "ScoreBoard.h"
#include "Game.h"
#include <string>
#include "PlayingArea.h"
#include <wx/graphics.h>

using namespace std;


/// Ready message
const wxString ReadyText = "Get Ready!";
/// Size to display "Get Ready" before the level begins
const int ReadySize = 20;
/// Leading zeros for Score
const int LeadingZeros = 6;


/// Y location to display "Get Ready" relative to the
/// vertical center of the scoreboard. The X value is
/// chosen that centers the text
const int ReadyY = -10;

/// Font size for the measure and beat indicator
const int BeatSize = 35;

/// Y location to display the measure and beat
/// relative to the vertical center of the scoreboard.
const int BeatsY = -25;

/// Font size to use to display the score
const int ScoreSize = 30;

/// Y location to display the score
/// relative to the vertical center of the scoreboard.
const int ScoreY = 15;

/**
 * Constructor
 * @param game Game this ScoreBoard is a member of
 */
ScoreBoard::ScoreBoard(Game *game) : Score(game)
{
}


/**
 * Draw the scoreboard
 * @param graphics the graphics object
 */
void ScoreBoard::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    Item::Draw(graphics);
    int currentBeat = GetGame()->GetPlayingArea()->GetCurrentBeat();
    int adjustedBeatCountingUp = currentBeat % GetGame()->GetPlayingArea()->GetBeatsPerMeasure() + 1;
    int measure = currentBeat / GetGame()->GetPlayingArea()->GetBeatsPerMeasure() + 1;
    double wid, hit;

    // ** Fonts ** //
    wxFont readyFont(wxSize(0, ReadySize),
                    wxFONTFAMILY_SWISS,
                    wxFONTSTYLE_NORMAL,
                    wxFONTWEIGHT_BOLD);
    wxFont beatFont(wxSize(0, BeatSize),
                    wxFONTFAMILY_SWISS,
                    wxFONTSTYLE_NORMAL,
                    wxFONTWEIGHT_BOLD);
    wxFont scoreFont(wxSize(0, ScoreSize),
                     wxFONTFAMILY_SWISS,
                     wxFONTSTYLE_NORMAL,
                     wxFONTWEIGHT_BOLD);
    if (GetGame()->GetPlayingArea()->GetCurrentBeat() == -GetGame()->GetPlayingArea()->GetBeatsPerMeasure()){
        graphics->SetFont(readyFont, wxColour(*wxBLACK));
        graphics->GetTextExtent(ReadyText, &wid, &hit);
        graphics->DrawText(ReadyText,
                           GetX() - wid/2,
                           GetY() + ReadyY - hit/2);
        return; // so it doesn't draw the score text
    }else if (GetGame()->GetPlayingArea()->GetCurrentBeat() > 0){
        // ** beat text ** //
        graphics->SetFont(beatFont, wxColour(*wxBLACK));
        graphics->GetTextExtent(to_string(measure) + ":" + to_string(adjustedBeatCountingUp), &wid, &hit);
        graphics->DrawText(to_string(measure) + ":" + to_string(adjustedBeatCountingUp),
                           GetX() - wid/2,
                           GetY() + BeatsY - hit/2);
    }else {
        double adjustedBeatCountingDown = (-currentBeat+1);
        // ** beat text as decreasing..  ** //
        graphics->SetFont(beatFont, wxColour(*wxBLACK));
        graphics->GetTextExtent(to_string(int(adjustedBeatCountingDown)), &wid, &hit);
        graphics->DrawText(to_string(int(adjustedBeatCountingDown)),
                           GetX() - wid / 2,
                           GetY() + BeatsY - hit / 2);
    }


    std::string formattedNumber = std::string(LeadingZeros - std::to_string(GetGame()->GetPlayingArea()->GetScore()).length(), '0') + std::to_string(GetGame()->GetPlayingArea()->GetScore());
    // ** score text ** //
    graphics->SetFont(scoreFont, wxColour(*wxBLACK));
    graphics->GetTextExtent(formattedNumber, &wid, &hit);
    graphics->DrawText(formattedNumber,
                       GetX() - wid/2,
                       GetY() + ScoreY - hit/2);

}
