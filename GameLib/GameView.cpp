/**
 * @file GameView.cpp
 * @author Abdulrahman Alanazi
 */

#include "pch.h"
#include "GameView.h"
#include "Key.h"
#include <wx/dcbuffer.h>
#include <wx/graphics.h>
#include "PlayingArea.h"
#include "Sound.h"
using namespace std;

/// Frame duration in milliseconds
const int FrameDuration = 1;

/**
 * Constructor
 * @param audioEngine The audio engine for the game
 */
GameView::GameView(ma_engine *audioEngine) : mGame(audioEngine) {

}

/**
 * Handle a key press event
 * @param event Keypress event
 */


void GameView::OnKeyDown(wxKeyEvent& event) {
    UpdateTime();

    int keyCode = event.GetKeyCode();

//    // Use mGame instance to call GetSoundByKeyCode
//    auto sound = mGame.GetSoundByKeyCode(keyCode);
//    if (sound) {
//        sound->PlaySound();
//    }

    mGame.KeyPressed(keyCode);
    Refresh();
}







/**
 * Handle a key release event
 * @param event Key release event
 */
void GameView::OnKeyUp(wxKeyEvent& event)
{
    UpdateTime();
    mGame.KeyUp(event.GetKeyCode());
    // This probably will be temporary because when we click a key we change
    // the size of the image to indicates that we pressed the key
}


void GameView::LoadGame(int levelNumber) {
    mGame.SetCurrentLevel(levelNumber);
    mGame.NewGame();
}

void GameView::Initialize(wxFrame* parent)
{
    Create(parent, wxID_ANY,
           wxDefaultPosition, wxDefaultSize,
           wxFULL_REPAINT_ON_RESIZE);
    SetBackgroundStyle(wxBG_STYLE_PAINT);
    Bind(wxEVT_PAINT, &GameView::OnPaint, this);
    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnExit, this, wxID_EXIT);
    Bind(wxEVT_KEY_DOWN, &GameView::OnKeyDown, this);
    Bind(wxEVT_KEY_UP, &GameView::OnKeyUp, this);
    Bind(wxEVT_TIMER, &GameView::OnTimer, this);
    parent->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::AutoPlayMode, this, IDM_AUTOPLAY);
    mTimer.SetOwner(this);
    mTimer.Start(FrameDuration,false);
    mStopWatch.Start();
    LoadGame(0); // Default level is 0
}
/**
 * Paint event, draws the window.
 * @param event Paint event object
 */
void GameView::OnPaint(wxPaintEvent& event)
{

    UpdateTime();

    // Create a double-buffered display context
    wxAutoBufferedPaintDC dc(this);

    // Clear the image to black
    wxBrush background(*wxBLACK);
    dc.SetBackground(background);
    dc.Clear();

    // Create a graphics context
    auto gc =
        std::shared_ptr<wxGraphicsContext>(wxGraphicsContext::Create(dc));

    // Tell the game class to draw
    wxRect rect = GetRect();

    mGame.OnDraw(gc, rect.GetWidth(), rect.GetHeight());

}
/**
 * Exit event.
 * @param event Paint event object
 */
void GameView::OnExit(wxCommandEvent& event)
{
    Close(true);
}
/**
 * Toggle auto play mode
 * @param event command event object
 */
void GameView::AutoPlayMode(wxCommandEvent& event)
{
    mGame.GetPlayingArea()->ToggleAutoPlay(event.IsChecked());
}
/**
 * Update the time since the last call to UpdateTime
 * and call update on the game.
 */
void GameView::UpdateTime()
{
    auto newTime = mStopWatch.Time();
    auto elapsed = (double)(newTime - mTime) * 0.001;
    mTime = newTime;
    mGame.Update(elapsed);
}

/**
 * Handle timer events
 * @param event timer event
 */
void GameView::OnTimer(wxTimerEvent& event)
{
    Refresh();
}