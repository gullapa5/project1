/**
 * @file ScoreBoard.h
 * @author Abdulrahman Alanazi
 */

#ifndef PROJECT1__SCOREBOARD_H
#define PROJECT1__SCOREBOARD_H

#include "Item.h"
#include "Score.h"
/**
 * handling the scoreboard for the score
 */
class ScoreBoard : public Score{
public:
    // Default constructor (disabled)
    ScoreBoard() = delete;
    /// Copy constructor (disabled)
    ScoreBoard(const ScoreBoard &) = delete;

    /// Assignment operator
    void operator=(const ScoreBoard &) = delete;


    void Draw(std::shared_ptr<wxGraphicsContext> graphics);



    ScoreBoard(Game* game);

    /**
    * Accept a visitor
    * @param visitor The visitor we accept
    */
    virtual void Accept(ItemVisitor* visitor) {visitor->VisitScoreBoard(this);};
private:
};

#endif //PROJECT1__SCOREBOARD_H
