project(GameLib)

set(SOURCE_FILES pch.h
        MainFrame.cpp
        MainFrame.h
        SoundBoard.h
        SoundBoard.cpp
        Sound.h
        Sound.cpp
        Puck.h
        Puck.cpp
        PlayingArea.h
        PlayingArea.cpp
        Meter.h
        Meter.cpp
        GameView.h
        GameView.cpp
        Game.h
        Game.cpp
        ids.h
        Key.h
        Key.cpp
        Track.h
        Track.cpp
        Key.h
        Item.cpp
        Item.h
        Score.cpp
        Score.h
        ScoreBoard.cpp
        ScoreBoard.h
        SoundBoardCover.cpp
        SoundBoardCover.h
        ItemVisitor.cpp
        ItemVisitor.h
        KeyVisitor.cpp
        KeyVisitor.h
        Image.cpp
        Image.h
        Needle.cpp
        Needle.h
        Declarations.cpp
        Declarations.h
        DeclarationImage.cpp
        DeclarationImage.h
        DeclerationSoundBoard.cpp
        DeclerationSoundBoard.h
        DeclerationScoreBoard.cpp
        DeclerationScoreBoard.h
        DeclarationMeter.cpp
        DeclarationMeter.h
        DeclarationPuck.cpp
        DeclarationPuck.h
        DeclarationMusic.cpp
        DeclarationMusic.h
        Note.cpp
        Note.h
        SoundBoardFinderVisitor.cpp
        SoundBoardFinderVisitor.h
        DeclarationVisitor.cpp
        DeclarationVisitor.h
        FindNoteDeclarationVisitor.cpp
        FindNoteDeclarationVisitor.h
        DeclarationAudio.cpp
        DeclarationAudio.h
        AudioManager.cpp
        AudioManager.h
        ScoreBoardFinderVisitior.cpp
        ScoreBoardFinderVisitior.h
)

set(wxBUILD_PRECOMP OFF)
find_package(wxWidgets COMPONENTS core base xrc html xml REQUIRED)

include(${wxWidgets_USE_FILE})



add_library(${PROJECT_NAME} STATIC ${SOURCE_FILES})

target_link_libraries(${PROJECT_NAME} ${wxWidgets_LIBRARIES})